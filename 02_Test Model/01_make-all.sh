#!/usr/bin/env bash
#
# Author:   Marco Ardizzone
# Email:    marco.ardizzone@studenti.polito.it
# Date:     March 2017
# Description:
#   This is a simple bash script for compiling the required files to run the
#   tests. It compiles the monitoring program and the loader program.
#   Calling the script with 'clean' parameter deletes the compiled files.
#################################################################################

MONITOR_SRC="monitor.c"
MONITOR_BIN="monitor.out"

LOADER_FOLDER="loader"
LOADER_SRC="executor.c"
LOADER_BIN="executor.out"

ROOT_DIR=$(pwd)

if [[ $1 == "clean" ]]; then
    rm "$ROOT_DIR/$MONITOR_BIN"
    rm "$ROOT_DIR/$LOADER_BIN"
else
    #Compile the monitor
    gcc -O3 -o $MONITOR_BIN $MONITOR_SRC

    #Compile the loader
    cd $LOADER_FOLDER
    gcc $LOADER_SRC -O3 -o $LOADER_BIN -lpthread
    mv $LOADER_BIN ../
fi