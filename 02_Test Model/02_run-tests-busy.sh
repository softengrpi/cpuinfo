#!/usr/bin/env bash
# Author:   Marco Ardizzone
# Email:    marco.ardizzone@studenti.polito.it
# Date:     March 2017
# Description:
#   This is the main script used to run the tests when the CPU is under a given
#   load.
#   The script takes care of doing a number of checks before starting sampling
#   data.
#   You need to provide three arguments to run the script: <dev_state>,
#   <root_psw> and <ntp_server_address>.
#   The program will check if the NTP program to synchronize time is installed on
#   the system and if so it will synchronize the time with the NTP server.
#   It will check then if the programs used to load the cpu and monitor it are
#   compiled.
#   If all the checks are successful, it will try to mount a ramdisk to /tmp/
#   folder.
#   Once this procedure is over, the tests will be run from run_test_order function,
#   which takes as input the test_type (a sorting algorithm), array_size and the
#   initial ordering of the test array. More information can be found in the
#   function header comment.
#   
#   run_test_order function takes care of checking if all the provided arguments are
#   valid and then will try synchronizing time again, turn devices off if needed
#   (see <dev_state> parameter), run the monitor in wait mode, run the loader,
#   start the monitor and stop the test when the loader program is finished.
#   
#   There is a number of configurable parameters down below, you can change them
#   as needed.
#   
#   Generally, in case of errors, the script should be able to stop everything
#   and restore the system state to its original condition (i.e turn HDMI,
#   ethernet and USB ports on if they were turned off).
#   Some instances of the launched programs may still be running in background if
#   an error occurs, so a system reboot is suggested.
#################################################################################
#
# DO NOT TOUCH
# This fetches the controller in charge of managing the usb and the ethernet. It's specific for the RPi
ETH_CONTROLLER=$(find /sys/devices/ -name `dmesg -t | grep dwc_otg | grep "DWC OTG Controller" | awk '{print $2}' | cut -d":" -f1`)
CUR_DIR=$(pwd)                  # The current working directory of the script
RAMDISK=                        # Path to where data will be temporarly saved
NTP_SERVER=                     # Local NTP server used to sync hour 
DEVS_STATE=                     # Wheter Ethernet, HDMI and USB are on or off
RUN_MONITOR=                    # Wheater to run the monitor program or not

# Change as needed
MONITOR_PROG="monitor.out"      # Program that monitors the cpu usage
LOADER_PROG="sysbench"          # Program used to load the CPU. Choices: sysbench, avconv, zip, openssl, executor.out
RUN_TIME=600                    # Seconds to run sysbench
VIDEO_FILE="burgers.mp4"        # Video to be used by the run_test_video, can be mp4 or mkv format
DUMMY_FILE="dummy.txt"          # File used for zip and openssl tests
TEST_TYPE_CORE="multi"         # Can be multi or single. multi = multicore test, single = single core test

function print_help(){
    case $1 in
        "args")
            echo
            echo "$0 <dev_state> <monitor_state> <psw> <ntp_server_address>"
            echo
            echo "Where:"
            echo "  - <dev_state>: on or off . If off HDMI, Ethernet and USB are turned off before starting the test."
            echo "  - <monitor_state>: on or off . If off monitor won't be run."
            echo "  - <psw>: password for super user"
            echo "  - <ntp_server_address>: IPv4 address of the local NTP server used to sync the time"
            echo
            ;;
        "monitor")
            echo
            echo "Monitor program $MONITOR_PROG was not found"
            echo "Maybe you need to compile it?"
            echo
            ;;
        "loader")
            echo
            echo "Loader program $LOADER_PROG was not found"
            echo "Maybe you need to compile or install it?"
            echo
            ;;
        "ntpdate")
            echo
            echo "ntpdate was not found"
            echo "Maybe you need to install it?"
            echo
            ;;
        "devs")
            echo
            echo "Unknown device state $DEVS_STATE"
            echo "Possible states are 'on' or 'off'"
            echo
            ;;
        "mon_state")
            echo
            echo "Unknown monitor state $RUN_MONITOR"
            echo "Possible states are 'on' or 'off'"
            echo
            ;;
        "video_file")
            echo
            echo "$VIDEO_FILE was not found"
            echo
            ;;
        "dummy_file")
            echo
            echo "$DUMMY_FILE was not found"
            echo
            ;;
        "test_type")
            echo
            echo "Unknown test type $TEST_TYPE_CORE for loader $LOADER_PROG"
            echo
            ;;
        *)
            echo
            echo "Something went terribly wrong"
            echo
            ;;
    esac
}

#Creates and mounts the ramdisk on which we'll save the results of the monitoring
function make_ramdisk(){
    RAMDISK="/tmp/ramdisk`date +%s`"
    mkdir $RAMDISK || return 1
    echo $1 | sudo -S mount -t ramfs -o size=128m ramfs $RAMDISK || return 1
    sudo chmod 777 $RAMDISK || return 1
    echo "Ramdisk mounted successfully at $RAMDISK"
    return 0
}

# The function takes as input:
#   - <test_type>: bubble - merge - quick - counting
#   - <array_size>: unsigned integer
#   - <order_type>: integer in range [1; 4] 
function run_test_order(){

    test_type=$1
    if [[ "$test_type" != "bubble" && "$test_type" != "merge" && "$test_type" != "quick" && "$test_type" != "counting" ]]; then
        echo "Unknown test type $test_type"
        echo "Possible tests are: bubble - merge - quick - counting"
        return 1
    fi

    array_size=$2

    order_type=$3
    order_type_char=
    if [[ "$order_type" -lt 1 || "$order_type" -gt 4 ]]; then
        echo "Array ordering must be in range [1; 4]"
        echo "Possible orderings are:"
        echo "  - 1 => sorted array"
        echo "  - 2 => reversed array"
        echo "  - 3 => random1"
        echo "  - 4 => random2"
        return 1
    else
        case $order_type in
            1 )
                order_type_char="o"
                ;;
            2 )
                order_type_char="r"
                ;;
            3 )
                order_type_char="r1"
                ;;
            4 )
                order_type_char="r2"
                ;;
        esac
    fi

    echo "Running test $test_type with $array_size elements. Ordering $order_type_char."

    # Sync time just before starting
    # If synching fails, it's ok. We've already synched before starting the tests,
    # so at least the error difference is minimum.
    echo $psw | sudo -S ntpdate -u $NTP_SERVER > /dev/null 2>&1

    if [[ "$DEVS_STATE" == "off" ]]; then
        set_eth_usb_controller "off"
        set_HDMI "off"
    fi

    if [[ "$RUN_MONITOR" == "on" ]]; then
        if [[ "$TEST_TYPE_CORE" == "multi" ]]; then
            ./"$MONITOR_PROG" "$RAMDISK" "${test_type}_${order_type_char}_${array_size}" "$DEVS_STATE" &
        elif [[ "$TEST_TYPE_CORE" == "single" ]]; then
            ./"$MONITOR_PROG" "$RAMDISK" "${test_type}" "${order_type_char}" "${array_size}" "$DEVS_STATE" "$LOADER_PROG" &
        fi
        monitor_pid=$(pgrep $MONITOR_PROG)
    fi

    # Executor Logfile is meaningful only when monitor is off. It will contain
    # the start and end time of the test, which will be used to take only the
    # right data from the DAQ logs.
    executor_logfile_name="${RAMDISK}/${test_type}_${array_size}_${order_type}.csv"
    if [[ "$RUN_MONITOR" == "off" ]]; then
        echo "Start time: $(date +%s)" >> $executor_logfile_name
    fi
    # In the case of the sorting algos, we perform a bit of string manipulation
    # to the output executor file. We pretty print it because if we'll ever need
    # it, we'll have a nicely printed output.
    # Here we also lunch the executor.
    echo "Run;Time" >> $executor_logfile_name
    if [[ "$TEST_TYPE_CORE" == "multi" ]]; then
        ./"$LOADER_PROG" "$test_type" "$array_size" "$order_type" | sed -E "s/Total execution time: ([0-9]+\.[0-9]+)/Total: \1/" | sed -E "s/Run ([0-9]+) - Time: ([0-9]+\.[0-9]+)/\1;\2/" | tee -a $executor_logfile_name &
    else
        # Here we need to convert the test_type to a string recognised by the single core application
        case $test_type in
            "quick")
                test_type="quickC"
                ;;
            "merge")
                test_type="mergeC"
                ;;
            "bubble")
                test_type="bubbleC"
                ;;
            "counting")
                test_type="countC"
                ;;
            *)
                echo "You shouldn't be here"
                exit 1
                ;;
        esac
        ./"$LOADER_PROG" "$test_type" "$array_size" "$order_type" | sed -E "s/Execution time for [a-zA-Z]+ [0-9]+ elements: [0-9]+ dataset ([0-9]+) run --> ([0-9]+\.[0-9]+)/\1;\2/"| sed -E "s/End//" | tee -a $executor_logfile_name &
    fi
    loader_pid=$(pgrep $LOADER_PROG)

    if [[ "$RUN_MONITOR" == "on" ]]; then
        #Start the monitor
        kill -s SIGUSR1 $monitor_pid
    fi

    # Keep checking if the test is over. If so, exit the while loop.
    # Test is over when pgrep doesn't return the loader pid.
    while [[ $(pgrep $LOADER_PROG) == $loader_pid ]]; do
        sleep 0.5
    done

    if [[ "$RUN_MONITOR" == "on" ]]; then
        #Stop the monitor
        kill -s SIGUSR2 $monitor_pid
        while [[ $(pgrep $MONITOR_PROG) == $monitor_pid ]]; do
            kill -s SIGUSR2 $monitor_pid
            sleep 0.5
        done
    else
        # The monitor is not running and the test is over, save the epoch to the
        # executor logfile.
        echo "End time: $(date +%s)" >> $executor_logfile_name
    fi    

    if [[ "$DEVS_STATE" == "off" ]]; then
        set_HDMI "on"
        set_eth_usb_controller "on"
    fi

    if mv $RAMDISK/* "$CUR_DIR/" ; then
        echo "All files copied to $CUR_DIR"
        echo
        echo
        return 0
    else
        echo "Copy of files failed, do it yourself."
        echo "Files are at $RAMDISK"
        echo
        echo
        return 1
    fi
}

function run_test_sysbench(){

    echo "Running $LOADER_PROG for $RUN_TIME - Bus $DEVS_STATE - Monitor $RUN_MONITOR"

    # Sync time just before starting
    # If synching fails, it's ok. We've already synched before starting the tests,
    # so at least the error difference is minimum.
    echo $psw | sudo -S ntpdate -u $NTP_SERVER > /dev/null 2>&1

    cores=$(nproc)
    # We can run sysbench even before starting the monitor. We don't care because
    # sysbench will run indefinetly and it's up to us to stop it.
    if [[ "$TEST_TYPE_CORE" == "multi" ]]; then
        ( sysbench --num-threads=$cores --test=cpu --cpu-max-prime=2000000 run & ) > /dev/null 2>&1
    elif [[ "$TEST_TYPE_CORE" == "single" ]]; then
        ( sysbench --num-threads=1 --test=cpu --cpu-max-prime=2000000 run & ) > /dev/null 2>&1
    else
        print_help "test_type"
        return 1
    fi
    loader_pid=$(pgrep $LOADER_PROG)

    if [[ "$DEVS_STATE" == "off" ]]; then
        set_eth_usb_controller "off"
        set_HDMI "off"
    fi

    # Executor Logfile is meaningful only when monitor is off. It will contain
    # the start and end time of the test, which will be used to take only the
    # right data from the DAQ logs.
    executor_logfile_name="${RAMDISK}/executor_${LOADER_PROG}_bus_${DEVS_STATE}_monitor_${RUN_MONITOR}.csv"
    if [[ "$RUN_MONITOR" == "on" ]]; then
        if [[ "$TEST_TYPE_CORE" == "multi" ]]; then
            ./"$MONITOR_PROG" "$RAMDISK" "$LOADER_PROG" "$DEVS_STATE" &
        else
            ./"$MONITOR_PROG" "$RAMDISK" "bubble" "o" "1" "$DEVS_STATE" "$LOADER_PROG" &
        fi
        monitor_pid=$(pgrep $MONITOR_PROG)
        kill -s SIGUSR1 $monitor_pid
    else
        echo "Start time: " >> $executor_logfile_name
        echo -n "$(date +%s);" >> $executor_logfile_name
    fi

    SECONDS=0
    # Keep checking if SECONDS is lower then RUN_TIME. If so, sleep,
    # else exit while loop.
    while [[ "$SECONDS" -lt  "$RUN_TIME" ]]; do
        sleep 1
    done

    if [[ "$RUN_MONITOR" == "on" ]]; then
        #Stop the monitor
        kill -s SIGUSR2 $monitor_pid
        while [[ $(pgrep $MONITOR_PROG) == $monitor_pid ]]; do
            kill -s SIGUSR2 $monitor_pid
            sleep 0.5
        done
    else
        # The monitor is not running and the test is over, save the epoch to the
        # executor logfile.
        echo -n "End time: " >> $executor_logfile_name
        echo "$(date +%s)" >> $executor_logfile_name
    fi    

    # Kill sysbench
    kill -s SIGTERM $loader_pid
    while [[ $(pgrep $loader_pid) == $loader_pid ]]; do
        kill -s SIGTERM $loader_pid
        sleep 0.5
    done

    if [[ "$DEVS_STATE" == "off" ]]; then
        set_HDMI "on"
        set_eth_usb_controller "on"
    fi

    if mv $RAMDISK/* "$CUR_DIR/" ; then
        echo "All files copied to $CUR_DIR"
        echo
        echo
        return 0
    else
        echo "Copy of files failed, do it yourself."
        echo "Files are at $RAMDISK"
        echo
        echo
        return 1
    fi
}

function run_test_video(){
    filename=$(basename "$VIDEO_FILE")
    extension="${filename##*.}"
    if [[ "$extension" == "mp4" ]]; then
        echo "Running $LOADER_PROG - $extension => mkv - Bus $DEVS_STATE - Monitor $RUN_MONITOR"
    elif [[ "$extension" == "mkv" ]]; then
        echo "Running $LOADER_PROG - $extension => mp4 - Bus $DEVS_STATE - Monitor $RUN_MONITOR"
    else
        echo "Unknown extension for $VIDEO_FILE."
        echo "SUpported file format are mp4 and mkv."
        return 1
    fi

    # Sync time just before starting
    # If synching fails, it's ok. We've already synched before starting the tests,
    # so at least the error difference is minimum.
    echo $psw | sudo -S ntpdate -u $NTP_SERVER > /dev/null 2>&1

    if [[ "$DEVS_STATE" == "off" ]]; then
        set_eth_usb_controller "off"
        set_HDMI "off"
    fi

    # Executor Logfile is meaningful only when monitor is off. It will contain
    # the start and end time of the test, which will be used to take only the
    # right data from the DAQ logs.
    executor_logfile_name="${RAMDISK}/executor_${LOADER_PROG}_bus_${DEVS_STATE}_monitor_${RUN_MONITOR}.csv"
    if [[ "$RUN_MONITOR" == "on" ]]; then
        ./"$MONITOR_PROG" "$RAMDISK" "$LOADER_PROG" "$DEVS_STATE" &
        monitor_pid=$(pgrep $MONITOR_PROG)
    fi
    echo "Start time: $(date +%s)" >> $executor_logfile_name

    for i in $(seq 1 30); do
        if [[ "$i" -eq 1 ]]; then
            echo "Run;Time" >> $executor_logfile_name
            if [[ "$RUN_MONITOR" == "on" ]]; then
                kill -s SIGUSR1 $monitor_pid
            fi
        fi
        echo -n "$i;" | tee -a $executor_logfile_name
        SECONDS=0
        if [[ "$extension" == "mp4" ]]; then
            (avconv -y -r ntsc-film -i "$VIDEO_FILE" outfile.mkv) > /dev/null 2>&1
            echo "$SECONDS" | tee -a $executor_logfile_name
        else
            (avconv -y -r ntsc-film -i "$VIDEO_FILE" -strict experimental outfile.mp4) > /dev/null 2>&1            
            echo "$SECONDS" | tee -a $executor_logfile_name
        fi
    done

    if [[ "$RUN_MONITOR" == "on" ]]; then
        #Stop the monitor
        kill -s SIGUSR2 $monitor_pid
        while [[ $(pgrep $MONITOR_PROG) == $monitor_pid ]]; do
            kill -s SIGUSR2 $monitor_pid
            sleep 0.5
        done
    fi
    # The test is over, save the epoch to the executor logfile.
    echo "End time: $(date +%s)" >> $executor_logfile_name

    if [[ "$DEVS_STATE" == "off" ]]; then
        set_HDMI "on"
        set_eth_usb_controller "on"
    fi

    if mv $RAMDISK/* "$CUR_DIR/" ; then
        echo "All files copied to $CUR_DIR"
        echo
        echo
        return 0
    else
        echo "Copy of files failed, do it yourself."
        echo "Files are at $RAMDISK"
        echo
        echo
        return 1
    fi

}

# Test can be:
#   - File zipping using zip
#   - File secure hashing using openssl
function run_generic_single_core_test(){
    
    echo "Running $LOADER_PROG - Bus $DEVS_STATE - Monitor $RUN_MONITOR"

    # Sync time just before starting
    # If synching fails, it's ok. We've already synched before starting the tests,
    # so at least the error difference is minimum.
    echo $psw | sudo -S ntpdate -u $NTP_SERVER > /dev/null 2>&1

    if [[ "$DEVS_STATE" == "off" ]]; then
        set_eth_usb_controller "off"
        set_HDMI "off"
    fi

    # Executor Logfile is meaningful only when monitor is off. It will contain
    # the start and end time of the test, which will be used to take only the
    # right data from the DAQ logs.
    executor_logfile_name="${RAMDISK}/executor_${LOADER_PROG}_bus_${DEVS_STATE}_monitor_${RUN_MONITOR}.csv"
    if [[ "$RUN_MONITOR" == "on" ]]; then
        ./"$MONITOR_PROG" "$RAMDISK" "$LOADER_PROG" "$DEVS_STATE" &
        monitor_pid=$(pgrep $MONITOR_PROG)
    fi
    echo "Start time: $(date +%s)" >> $executor_logfile_name

    for i in $(seq 1 30); do
        if [[ "$i" -eq 1 ]]; then
            echo "Run;Time" >> $executor_logfile_name
            if [[ "$RUN_MONITOR" == "on" ]]; then
                kill -s SIGUSR1 $monitor_pid
            fi
        fi
        echo -n "$i;" | tee -a $executor_logfile_name
        SECONDS=0
        case $LOADER_PROG in
            "zip")
                zip -1 zipped.zip $DUMMY_FILE > /dev/null 2>&1
                echo "$SECONDS" | tee -a $executor_logfile_name
                rm zipped.zip
                ;;
            "openssl")
                # Double hashing like blockchain
                openssl dgst -sha256 $DUMMY_FILE | openssl dgst -sha256 > /dev/null 2>&1
                echo "$SECONDS" | tee -a $executor_logfile_name
                ;;
            *)
                echo "You shouldn't be here"
                return 1
                ;;
        esac
    done

    if [[ "$RUN_MONITOR" == "on" ]]; then
        #Stop the monitor
        kill -s SIGUSR2 $monitor_pid
        while [[ $(pgrep $MONITOR_PROG) == $monitor_pid ]]; do
            kill -s SIGUSR2 $monitor_pid
            sleep 0.5
        done
    fi
    # The test is over, save the epoch to the executor logfile.
    echo "End time: $(date +%s)" >> $executor_logfile_name

    if [[ "$DEVS_STATE" == "off" ]]; then
        set_HDMI "on"
        set_eth_usb_controller "on"
    fi

    if mv $RAMDISK/* "$CUR_DIR/" ; then
        echo "All files copied to $CUR_DIR"
        echo
        echo
        return 0
    else
        echo "Copy of files failed, do it yourself."
        echo "Files are at $RAMDISK"
        echo
        echo
        return 1
    fi
}

function set_HDMI(){
    if [[ $1 == "off" ]]; then
        echo -n "Disabling HDMI.. "
        sleep 10
        echo $psw | sudo -S /opt/vc/bin/tvservice -o > /dev/null 2>&1
        echo "Done"
    elif [[ $1 == "on" ]]; then
        echo $psw | sudo -S /opt/vc/bin/tvservice -p > /dev/null 2>&1
        # The following is a hack found https://www.raspberrypi.org/forums/viewtopic.php?t=16472&p=176258
        # The problem with reactivating the HDMI is that the screen remains blank even if blanking is
        # turned off with setterm -blank 0, pressing any key does nothing.
        # So chvt changes the foreground console to 6 (a random one) and back to 1 (the one used on the RPi
        # HDMI terminal output).
        echo $psw | sudo -S chvt 6
        echo $psw | sudo -S chvt 1
        echo "HDMI enabled"
    else
        echo "Wrong HDMI set parameter. Supply either 'on' or 'off'"
        return 1
    fi
    return 0
}

function set_eth_usb_controller(){
    if [[ $1 == "off" ]]; then
        echo -n "Stopping controller.."
        echo $psw | sudo -S ip link set eth0 down > /dev/null 2>&1
        echo $psw | sudo -S /etc/init.d/networking stop > /dev/null 2>&1
        echo 0 | sudo tee $ETH_CONTROLLER/buspower > /dev/null 2>&1
        echo "Controller stopped"
    elif [[ $1 == "on" ]]; then
        echo -n "Enabling controller.."
        echo 1 | sudo tee $ETH_CONTROLLER/buspower > /dev/null 2>&1
        sleep 2
        echo $psw | sudo -S /etc/init.d/networking start > /dev/null 2>&1
        echo $psw | sudo -S ip link set eth0 up > /dev/null 2>&1
        echo "Controller enabled"
    else
        echo "Wrong Ethernet+USB controller set parameter. Supply either 'on' or 'off'"
        return 1
    fi
    return 0
}

#If the arguments are not 4 print help and exit
if [[ "$#" -ne 4 ]]; then
    print_help "args"
    exit 1
else
    DEVS_STATE=$1   #Save the state of the devices
    if [[ "$DEVS_STATE" != "on" && "$DEVS_STATE" != "off" ]]; then
        print_help "devs"
        exit 2
    fi

    RUN_MONITOR=$2  #Save the state of the monitor
    if [[ "$RUN_MONITOR" != "on" && "$RUN_MONITOR" != "off" ]]; then
        print_help "mon_state"
        exit 9
    fi

    psw=$3          #Save the root password
    NTP_SERVER=$4   #Save the NTP server address

    #Check if loader program is there
    if [[ "$LOADER_PROG" == "sysbench" ]]; then
        type $LOADER_PROG >/dev/null 2>&1 || { print_help "loader"; exit 3; }
    elif [[ "$LOADER_PROG" == "avconv" ]]; then
        type $LOADER_PROG >/dev/null 2>&1 || { print_help "loader"; exit 3; }
        if [[ ! -e "$VIDEO_FILE" ]]; then
            print_help "video_file"
            exit 3
        fi
    elif [[ "$LOADER_PROG" == "openssl" || "$LOADER_PROG" == "zip" ]]; then
        type $LOADER_PROG >/dev/null 2>&1 || { print_help "loader"; exit 3; }
        if [[ ! -e "$DUMMY_FILE" ]]; then
            print_help "dummy_file"
            exit 3
        fi
    elif [[ ! -e "$LOADER_PROG" ]]; then
        print_help "loader"
        exit 3
    fi

    #Check if ntpdate is installed, if not exit
    type ntpdate >/dev/null 2>&1 || { print_help "ntpdate"; exit 4;}
    #Kill ntp daemon so there's no interference in the epoch sync
    echo $psw | sudo -S kill -s SIGTERM $(pgrep ntpd)
    #Check if the NTP server is valid and reachable, if not exit
    echo $psw | sudo -S ntpdate -u $NTP_SERVER > /dev/null 2>&1 || { echo "Cannot find NTP server $NTP_SERVER"; exit 5; }

    # If the monitor program is not found, print help and exit
    if [[ ! -e "$MONITOR_PROG" ]]; then
        print_help "monitor"
        exit 6
    fi

    #Make the ramdisk
    make_ramdisk $psw || { echo "Ramdisk creation failed!"; exit 7; }
    #Disable screen blanking
    setterm -blank 0
fi

#Run all the needed test with different load. In case of errors, stop and exit
if [[ "$LOADER_PROG" == "sysbench" ]]; then
    run_test_sysbench || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
elif [[ "$LOADER_PROG" == "avconv" ]]; then
    run_test_video || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
elif [[ "$LOADER_PROG" == "zip" || "$LOADER_PROG" == "openssl" ]]; then
    run_generic_single_core_test || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
else
    if [[ "$TEST_TYPE_CORE" == "multi" ]]; then
        #|--------------------------------QUICK SORT-------------------------------|#
        run_test_order "quick" 6000000 2 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
        run_test_order "quick" 6000000 3 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
        run_test_order "quick" 6000000 4 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }

        #|--------------------------------MERGE SORT-------------------------------|#
        run_test_order "merge" 8000000 2 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
        run_test_order "merge" 8000000 3 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
        run_test_order "merge" 8000000 4 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }

        #|-------------------------------BUBBLE SORT-------------------------------|#
        run_test_order "bubble" 50000 2 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
        run_test_order "bubble" 50000 3 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
        run_test_order "bubble" 50000 4 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }

        #|------------------------------COUNTING SORT------------------------------|#
        run_test_order "counting" 40000000 2 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
        run_test_order "counting" 40000000 3 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
        run_test_order "counting" 40000000 4 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
    elif [[ "$TEST_TYPE_CORE" == "single" ]]; then
        #|--------------------------------QUICK SORT-------------------------------|#
        run_test_order "quick" 21000 2 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
        run_test_order "quick" 5000000 3 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
        run_test_order "quick" 5000000 4 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }

        #|--------------------------------MERGE SORT-------------------------------|#
        run_test_order "merge" 4000000 2 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
        run_test_order "merge" 3500000 3 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
        run_test_order "merge" 3500000 4 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }

        #|-------------------------------BUBBLE SORT-------------------------------|#
        run_test_order "bubble" 185000 2 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
        run_test_order "bubble" 21500 3 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
        run_test_order "bubble" 23500 4 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }

        #|------------------------------COUNTING SORT------------------------------|#
        run_test_order "counting" 19000000 2 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
        run_test_order "counting" 8000000 3 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
        run_test_order "counting" 8000000 4 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 8; }
    else
        print_help "test_type"
        exit 9
    fi
fi
