/**
 * Author:  Marco Ardizzone
 * Email:   marco.ardizzone@studenti.polito.it
 * Date:    20-04-17
 * Description:
 *     This is the C program used to monitor CPU usage once the test are run from
 *     02_run-tests-*.sh scripts.
 *     The program takes as input 3 parameters: ramdisk path, loader id and bus
 *     state.
 *     The monitor is controlled with Linux signals: this is done to reduce as
 *     much as possible the overhead of the check operations on the input
 *     parameters. The signals used to control the monitor are:
 *         - SIGUSR1 to start the sampling
 *         - SIGUSR2 to stop the sampling, save data and quit.
 *     Once the program is started, it performs a number of checks on the input
 *     parameters and if all the checks are successful, it pauses itself with
 *     pause() syscall, waiting for SIGUSR1 signals.
 *     When SIGUSR1 is received, sample_cpu_data function is called and the
 *     sampling of CPU usage starts.
 *     The function has an initial comment explaining what type of calculations
 *     are performed, so take a look over there.
 *     The while loop you find in sample_cpu_data is where the magic happens. It
 *     should be noted that to keep file pointers in a consistent state, SIGUSR2
 *     is detected only at the end of the cycle. This approach requires a bit of
 *     spamming of SIGUSR2 from the outside to this program to ensure it is
 *     caught and the monitoring is stopped.
 *     If SIGUSR2 is ignored the monitoring will keep going and the subsequent
 *     tests will likely fail silently (i.e, no sampling will be performed).
 *     The 02_run-tests-*.sh scripts are designed to overcome this issue (see 
 *     the while loop containing the kill call), so you should be fine.
 *     
 *     Note that the program samples also the current frequency of the cpu cores:
 *     this sampling is specific to the Raspberry Pi 2 and should be changed
 *     according to the system you are testing.
 *
 * Compilation:
 *     For optimization purposes, the program should be compiled with -O3 flag.
 *         gcc -O3 -o monitor.out monitor.c
 *     However, you should not compile the program by yourself but use
 *     01_make-all.sh script which will take care of everything.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include <signal.h>

#define STR_LENGHT 80
#define MAX_CORES 4
#define nS_SLEEP 250 * 1000000
typedef struct timespec ttype;
typedef struct timeval timeval;

static const char APP_NAME[] = "monitor";


void print_help(int);
int make_ramdisk(char*, char[]);
void sample_cpu_data();
void close_ptrs();

char ramdisk_path[STR_LENGHT] = {0};
char file_id[STR_LENGHT] = {0};
FILE* fp_pipe_stat;
FILE* fp_pipe_freq;
FILE* fp_file;

int main(int argc, char* argv[]) {

    if(argc != 4 ){
        print_help(0);
        return -1;
    }
    
    if(strlen(argv[2]) < 1){
        print_help(1);
        return -1;
    }

    if(strncmp(argv[3], "on", STR_LENGHT) != 0 &&
       strncmp(argv[3], "off", STR_LENGHT) != 0){
        print_help(2);
        return -1;
    }

    //Save the name of the loader and device state
    snprintf(file_id, STR_LENGHT, "%s_%s", argv[2], argv[3]);

    //Save the path to ramdisk
    snprintf(ramdisk_path, STR_LENGHT, "%s", argv[1]);

    signal(SIGUSR1, sample_cpu_data);
    printf("Waiting for start signal..\n");
    pause();

    return 0;
}

/**
 * Prints to stdin a help message
 */
void print_help(int error){
    switch(error){
        case 0:
            printf("Error: Not enough parameters\n\nUsage: %s <ramdisk> <loader_id> <dev_state>\n", APP_NAME);
            printf("\t- <ramdisk>: path to a ramdisk\n");
            printf("\t- <loader_id>: A string identifying the loader used\n");
            printf("\t- <dev_state>: Indicates if HDMI and USB bus are turned on or off.\nAllowed values are 'on' or 'off'\n");
            break;
        case 1:
            printf("Error: Loader ID cannot be empty.\n");
            break;
        case 2:
            printf("Error: Unknown dev_state supplied.\n");
            printf("Allowed values: 'on' or 'off'\n");
            break;
        default:
            printf("Something went so very terribly wrong...\n");
            break;
    }

}


/**
 * Reads from /proc/stat user, nice, system busy cycles and idle cycles.
 * Computes the utilization
 *                    (C_user_t+C_nice_t+C_system_t - C_user_t_1+C_nice_t_1+C_system_t_1)
 *     u(t) = ---------------------------------------------------------------------------------------
 *            (C_user_t+C_nice_t+C_system_t+C_idle_t - C_user_t_1+C_nice_t_1+C_system_t_1+C_idle_t_1)
 * u(t) is then saved onto a file in csv format.
 * 
 */
void sample_cpu_data(){
    //Stop reacting to SIGUSR1
    signal(SIGUSR1, SIG_IGN);
    printf("Start sampling..\n");

    int i;
    ttype sleep_time;
    sleep_time.tv_sec = 0;
    sleep_time.tv_nsec = nS_SLEEP;
    timeval epoch;
    char data_filename[STR_LENGHT*2] = {0};
    unsigned long iterations = 0;
    unsigned long user_cycles_old = 0;
    unsigned long nice_cycles_old = 0;
    unsigned long system_cycles_old = 0;
    unsigned long idle_cycles_old = 0;
    //This should be changed according to the number of cores
    char* frequency_commands[MAX_CORES] = { "cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq",
                                            "cat /sys/devices/system/cpu/cpu1/cpufreq/scaling_cur_freq",
                                            "cat /sys/devices/system/cpu/cpu2/cpufreq/scaling_cur_freq",
                                            "cat /sys/devices/system/cpu/cpu3/cpufreq/scaling_cur_freq"};
    double frequencies[MAX_CORES] = {0};

    snprintf(data_filename, strlen(ramdisk_path)+STR_LENGHT, "%s/cpu_data_dump_%s.csv", ramdisk_path, file_id);
    if((fp_file=fopen(data_filename, "w")) == NULL){
        printf("error: Cannot create file %s to save dumps\n", data_filename);
        return;
    }
    while(1){
        /**
         * We ignore SIGUSR2 at the beginning of each iteration so that we can
         * complete a whole cycle without being interrupted.
         * This helps in keeping file pointers (fp_pipe_stat mainly) in a consistent
         * state.
         */
        signal(SIGUSR2, SIG_IGN);
        unsigned long user_cycles = 0;
        unsigned long nice_cycles = 0;
        unsigned long system_cycles = 0;
        unsigned long idle_cycles = 0;
        if ((fp_pipe_stat=popen("cat /proc/stat", "r")) == NULL) {
            printf("error: Cannot open pipe for 'cat /proc/stat' command\n");
            return;
        }
        fscanf(fp_pipe_stat, "%*s %lu %lu %lu %lu", &user_cycles, &nice_cycles, &system_cycles, &idle_cycles);
        for (i = 0; i < MAX_CORES; ++i) {
            if ((fp_pipe_freq=popen(frequency_commands[i], "r")) == NULL) {
                printf("error: Cannot open pipe for %s command\n", frequency_commands[i]);
                return;
            }
            fscanf(fp_pipe_freq, "%lf", &frequencies[i]);
            fclose(fp_pipe_freq);
        }
        gettimeofday(&epoch, NULL);
        if (iterations == 0){ //If it's the first iteration, we cannot compute the utilization as we have no previous data
            fprintf(fp_file, "Time (s);Iteration;Utilization(t);Frequency 0(KHz);Frequency 1(KHz); Frequency 2(KHz); Frequency 3(KHz)\n\n");
            fprintf(fp_file, "%lu.%03lu;%lu;0.0;%f;%f;%f;%f\n", epoch.tv_sec, epoch.tv_usec/1000, iterations, frequencies[0], frequencies[1], frequencies[2], frequencies[3]);
        }
        else {
            double utilization = 0.0;
            double busy_tot = (user_cycles+nice_cycles+system_cycles);
            double busy_tot_old = (user_cycles_old+nice_cycles_old+system_cycles_old);
            utilization = (double)(busy_tot - busy_tot_old)/(double)((busy_tot+idle_cycles)-(busy_tot_old+idle_cycles_old));
            fprintf(fp_file, "%lu.%03lu;%lu;%f;%f;%f;%f;%f\n", epoch.tv_sec, epoch.tv_usec/1000, iterations, utilization, frequencies[0], frequencies[1], frequencies[2], frequencies[3]);
        }
        fclose(fp_pipe_stat);
        /**
         * Once we close fp_pipe_stat we know we have a complete cycle.
         * This is the only window in which we can be stopped.
         */
        signal(SIGUSR2, close_ptrs);
        iterations++;
        user_cycles_old = user_cycles;
        nice_cycles_old = nice_cycles;
        system_cycles_old = system_cycles;
        idle_cycles_old = idle_cycles;
        nanosleep(&sleep_time, NULL);
    }
}

void close_ptrs(){
    signal(SIGUSR2, SIG_IGN);
    fclose(fp_file);
    raise(SIGTERM);
}