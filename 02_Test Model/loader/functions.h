
#define SEED1 1299827
#define SEED2 2199286
#define MAX_ARRAY_SIZE 1000000001   //Do not set this bigger than UINT_MAX
#define MAX_STR_LENGTH 21
#define MAX_RUNS 30                 //# of runs to perform
#define MAX_THREADS 4               //# of threads to use

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h> 
#include <pthread.h>

static const char APP_NAME[] = "executor.out";

typedef struct thread_args{
    short* array;
    unsigned int size;
} targs;

short* gen_array(unsigned int, unsigned short);
void print_array(short*, unsigned int);
long current_time(void);
float get_time_diff(long start, long end);
void sort(char[], short*, unsigned int);
short* clone_array(short*, unsigned int);

void* qsort_wrapper(void*);
int qsort_cmp(const void* a, const void* b);

void* msort_wrapper(void*);
void msort(short*, unsigned int, unsigned int, short*);
void merge(short*, unsigned int, unsigned int, unsigned int, short*);

void* bsort_wrapper(void*);
void bsort(short*, unsigned int);

void* csort_wrapper(void*);
void csort(short*, unsigned int);

short* gen_array(unsigned int size, unsigned short type){
    short* output_ar = NULL;
    unsigned int i = 0;

    output_ar = (short*) calloc(size, sizeof(short));
    if( output_ar != NULL){
            switch(type){
                case 1: //Sorted
                    for (i = 0; i < size; ++i){
                        if (i <= SHRT_MAX - 1)
                            output_ar[i] = SHRT_MIN+i;
                        else
                            output_ar[i] = SHRT_MAX;
                    }
                    break;
                case 2: //Reversed
                    for (i = 0; i < size; ++i){
                        if( i <= SHRT_MAX - 1)
                            output_ar[i] = SHRT_MAX - i;
                        else
                            output_ar[i] = SHRT_MIN;
                    }
                    break;
                case 3: //Random 1
                    srand(SEED1);
                    for (i = 0; i < size; ++i){
                        output_ar[i] = (rand() % ((2*SHRT_MAX)+1)) - SHRT_MIN;
                    }
                    break;
                case 4: //Random 2
                    srand(SEED2);
                    for (i = 0; i < size; ++i){
                        output_ar[i] = (rand() % ((2*SHRT_MAX)+1)) - SHRT_MIN;
                    }
                    break;
                default: //We shouldn't be here
                    return NULL;
            }
    }
    return output_ar;
}

void print_array(short* array, unsigned int size){
    unsigned int i=0;
    for(i=0; i < size; ++i){
        if(((i+1) % 10) == 0){
            printf("%hd\n", array[i]);
        }
        else{
            printf("%hd\t", array[i]);
        }
    }
    printf("\n");
}

long current_time() {
    struct timeval tp;
    
    gettimeofday(&tp,NULL);
    long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    
    return ms;
}

float get_time_diff(long start, long end){
    return (float) ((end-start)/1000.0);
}

void sort(char algo[], short* array, unsigned int size){
    short* copy = NULL;
    pthread_t t_ids[MAX_THREADS] = {0};

    unsigned int split_size = 0;
    unsigned int remainder = 0;
    unsigned short j, i;
    split_size = size / MAX_THREADS;
    for ( i= 0; i < MAX_RUNS; ++i){
        targs thread_arguments[MAX_THREADS];
        copy = clone_array(array, size);
        long start = current_time();
        if((remainder=size % MAX_THREADS) == 0){
            if(strncmp(algo, "quick", MAX_STR_LENGTH) == 0){
                for(j = 0; j < MAX_THREADS; ++j){
                    thread_arguments[j].size = split_size;
                    thread_arguments[j].array = &copy[j*split_size];
                    pthread_create(&t_ids[j], NULL, qsort_wrapper, &thread_arguments[j]);
                }
                for(j = 0; j < MAX_THREADS; ++j){
                    pthread_join(t_ids[j], NULL);
                }
            }
            else if(strncmp(algo, "merge", MAX_STR_LENGTH) == 0){
                for(j = 0; j < MAX_THREADS; ++j){
                    if(j == MAX_THREADS - 1){
                        thread_arguments[j].size = split_size+remainder;
                        thread_arguments[j].array = &copy[j*(split_size)];
                    }
                    else{
                        thread_arguments[j].size = split_size;
                        thread_arguments[j].array = &copy[j*split_size];
                    }
                    pthread_create(&t_ids[j], NULL, msort_wrapper, &thread_arguments[j]);
                }
                for(j = 0; j < MAX_THREADS; ++j){
                    pthread_join(t_ids[j], NULL);
                }
            }
            else if(strncmp(algo, "bubble", MAX_STR_LENGTH) == 0){
                for(j = 0; j < MAX_THREADS; ++j){
                    if(j == MAX_THREADS - 1){
                        thread_arguments[j].size = split_size+remainder;
                        thread_arguments[j].array = &copy[j*(split_size)];
                    }
                    else{
                        thread_arguments[j].size = split_size;
                        thread_arguments[j].array = &copy[j*split_size];
                    }
                    pthread_create(&t_ids[j], NULL, bsort_wrapper, &thread_arguments[j]);
                }
                for(j = 0; j < MAX_THREADS; ++j){
                    pthread_join(t_ids[j], NULL);
                }
            }
            else if(strncmp(algo, "counting", MAX_STR_LENGTH) == 0){
                for(j = 0; j < MAX_THREADS; ++j){
                    if(j == MAX_THREADS - 1){
                        thread_arguments[j].size = split_size+remainder;
                        thread_arguments[j].array = &copy[j*(split_size)];
                    }
                    else{
                        thread_arguments[j].size = split_size;
                        thread_arguments[j].array = &copy[j*split_size];
                    }
                    pthread_create(&t_ids[j], NULL, csort_wrapper, &thread_arguments[j]);
                }
                for(j = 0; j < MAX_THREADS; ++j){
                    pthread_join(t_ids[j], NULL);
                }
            }
        }
        else{//Array size is odd
            if(strncmp(algo, "quick", MAX_STR_LENGTH) == 0){
                for(j = 0; j < MAX_THREADS; ++j){
                    if(j == MAX_THREADS - 1){
                        thread_arguments[j].size = split_size+remainder;
                        thread_arguments[j].array = &copy[j*(split_size)];
                    }
                    else{
                        thread_arguments[j].size = split_size;
                        thread_arguments[j].array = &copy[j*split_size];
                    }
                    pthread_create(&t_ids[j], NULL, qsort_wrapper, &thread_arguments[j]);
                }
                for(j = 0; j < MAX_THREADS; ++j){
                    pthread_join(t_ids[j], NULL);
                }
            }
            else if(strncmp(algo, "merge", MAX_STR_LENGTH) == 0){
                for(j = 0; j < MAX_THREADS; ++j){
                    if(j == MAX_THREADS - 1){
                        thread_arguments[j].size = split_size+remainder;
                        thread_arguments[j].array = &copy[j*(split_size)];
                    }
                    else{
                        thread_arguments[j].size = split_size;
                        thread_arguments[j].array = &copy[j*split_size];
                    }
                    pthread_create(&t_ids[j], NULL, msort_wrapper, &thread_arguments[j]);
                }
                for(j = 0; j < MAX_THREADS; ++j){
                    pthread_join(t_ids[j], NULL);
                }
            }
            else if(strncmp(algo, "bubble", MAX_STR_LENGTH) == 0){
                for(j = 0; j < MAX_THREADS; ++j){
                    if(j == MAX_THREADS - 1){
                        thread_arguments[j].size = split_size+remainder;
                        thread_arguments[j].array = &copy[j*(split_size)];
                    }
                    else{
                        thread_arguments[j].size = split_size;
                        thread_arguments[j].array = &copy[j*split_size];
                    }
                    pthread_create(&t_ids[j], NULL, bsort_wrapper, &thread_arguments[j]);
                }
                for(j = 0; j < MAX_THREADS; ++j){
                    pthread_join(t_ids[j], NULL);
                }
            }
            else if(strncmp(algo, "counting", MAX_STR_LENGTH) == 0){
                for(j = 0; j < MAX_THREADS; ++j){
                    if(j == MAX_THREADS - 1){
                        thread_arguments[j].size = split_size+remainder;
                        thread_arguments[j].array = &copy[j*(split_size)];
                    }
                    else{
                        thread_arguments[j].size = split_size;
                        thread_arguments[j].array = &copy[j*split_size];
                    }
                    pthread_create(&t_ids[j], NULL, csort_wrapper, &thread_arguments[j]);
                }
                for(j = 0; j < MAX_THREADS; ++j){
                    pthread_join(t_ids[j], NULL);
                }
            }
        }
        csort(copy, size);
        long end = current_time();
        free(copy);
        printf("Run %u - Time: %f\n", i+1, get_time_diff(start, end));
    }
}

short* clone_array(short* array, unsigned int size){
    short* copy = (short*) calloc(size, sizeof(short));
    unsigned int i;
    for(i=0; i < size; ++i){
        copy[i] = array[i];
    }
    return copy;
}

void* qsort_wrapper(void* args){
    targs* thread_args = (targs*) args;
    qsort(thread_args->array, thread_args->size, sizeof *thread_args->array, &qsort_cmp);
    return NULL;
}

int qsort_cmp(const void* a, const void* b){
    short v1 = *(const short*) a;
    short v2 = *(const short*) b;

    return v1-v2;
}

void* msort_wrapper(void* args){
    targs* thread_args = (targs*) args;
    short* aux = (short*) calloc(thread_args->size, sizeof(short));
    msort(thread_args->array, 0, thread_args->size - 1, aux);
    free(aux);
    return NULL;
}

void msort(short* input, unsigned int start_pos, unsigned int end_pos, short* aux){
    unsigned int mid;

    if(start_pos < end_pos){
        mid = (start_pos+end_pos)/2;
        msort(input, start_pos, mid, aux);
        msort(input, mid+1, end_pos, aux);
        merge(input, start_pos, mid, end_pos, aux);
    }
}

void merge(short* input, unsigned int start_pos, unsigned int mid, unsigned int end_pos, short* aux){
    
    unsigned int i=start_pos, j=mid+1, k=start_pos;
    
    while(i <= mid && j <= end_pos){
        if (input[i] < input[j] )
            aux[k++] = input[i++];
        else
            aux[k++] = input[j++];        
    }

    while (i <= mid)
        aux[k++] = input[i++];
    while (j<=end_pos)
        aux[k++] = input[j++];
    for(k=start_pos; k<=end_pos; k++)
        input[k] = aux[k];
}

void* bsort_wrapper(void* args){
    targs* thread_args = (targs*) args;
    bsort(thread_args->array, thread_args->size);
    return NULL;
}

void bsort(short* array, unsigned int size){
    unsigned short i, j;
    unsigned short flag=1;
    
    for (i=0; i < size-1 && flag==1; i++) {
        flag = 0;
        for (j = 0; j < size-1-i; j++){
            if (array[j] > array[j+1]) {
                flag = 1;
                array[j] ^= array[j+1];
                array[j+1] ^= array[j];
                array[j] ^= array[j+1];   
            }
        }
    }    
}

void* csort_wrapper(void* args){
    targs* thread_args = (targs*) args;
    csort(thread_args->array, thread_args->size);
    return NULL;    
}

void csort(short* array, unsigned int size){
    unsigned int i, j=0;
    short* C;
    short min = array[0];
    short max = array[0];

    //Calculate min and max elements
    for(i=1; i < size; ++i){
        if (array[i] > max)
            max = array[i];
        else if(array[i] < min)
            min = array[i];
    }

    //Make an array that is max-min+1 in size
    //Also, initialize the array to 0
    C = (short*) calloc(max - min + 1, sizeof(short));
    for(i=0; i < size; ++i){
        C[array[i] - min]++;
    }

    for(i=0; i < max - min + 1; ++i){
        while(C[i] > 0){
            array[j] = i + min;
            j++;
            C[i]--;
        }
    }
    free(C);
}