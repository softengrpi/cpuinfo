/**
 * Author:		Marco Ardizzone
 * Email:		marco.ardizzone@studenti.polito.it
 * Date: 		April 2017
 * Description:
 */

#include "functions.h"

int main(int argc, char* argv[]) {

	unsigned long array_size=0;
	unsigned short array_type=0;
	short* array = NULL;
	char sort_algo[MAX_STR_LENGTH] = {0};
	long begin, end;

	if (argc != 4) {
		printf("Usage: ./%s <algorithm> <array_size> <array_order>\n", APP_NAME);
		printf("\t<algorithm>: quick - bubble - counting - merge\n");
		printf("\t<array_size>: An unsigned integer in range [1; %u]\n", MAX_ARRAY_SIZE-1);
		printf("\t<array_order>: An unsigned integer in range [1; 4]:\n");
		printf("\t\t1 => Sorted array\n");
		printf("\t\t2 => Reversed array\n");
		printf("\t\t3 => Random sorted 1 array\n");
		printf("\t\t4 => Random sorted 2 array\n");
		return -1;
	}

	strncpy(sort_algo, argv[1], MAX_STR_LENGTH-1);
	if(strncmp(sort_algo, "quick", MAX_STR_LENGTH) != 0 &&
	   strncmp(sort_algo, "merge", MAX_STR_LENGTH) != 0 &&
	   strncmp(sort_algo, "bubble", MAX_STR_LENGTH) != 0 &&
	   strncmp(sort_algo, "counting", MAX_STR_LENGTH) != 0){
		printf("Sorting algorithm '%s' is not supported.\n", sort_algo);
		return -1;
	}

	array_size = strtoul(argv[2], NULL, 10);
	if( array_size < 1 || array_size >= MAX_ARRAY_SIZE || argv[2][0] == '-') {
		printf("array_size '%s' is either out of range or cannot be converted to unsigned long\n", argv[2]);
		return -1;
	}

	array_type = (unsigned short) strtoul(argv[3], NULL, 10);
	if (array_type < 1 || array_type > 4 || argv[3][0] == '-'){
		printf("array_type '%s' is either out of range or cannot be converted to unsigned short\n", argv[3]);
		return -1;
	}

	array = gen_array(array_size, array_type);
	if(array == NULL){
		printf("Error: Failed allocating array\n");
		return -1;
	}

	begin = current_time();
	sort(sort_algo, array, array_size);
	end = current_time();

	free(array);
	printf("Total execution time: %f\n", get_time_diff(begin, end));

	return 0;
}
