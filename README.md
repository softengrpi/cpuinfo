#Raspberry Pi CPU Test

This repository contains a set of script to load and monitor the CPU usage of the Raspberry Pi 2. Once the data is collected with these tools, it can be merged with data collected from DAQ and saved onto a new CSV file listing power consumption vs. cpu usage. This file is then used to produce an analysis and have some plots representing the power consumption vs. cpu usage. 

## Data Acquisition
Once you clone this repo, you need to first compile everything on the target system. You can do this by running `make-all.sh` script. It will compile the following programs:

  + **cpulimit:** Is used to limit system-wide the maximum amount of resources the system can use.
  + **monitor.c:** Is used to parse the output of `/proc/stat` and get the CPU usage.

The tests can be run by launching `run-tests.sh` from shell. This program takes as input the *root password* which is used to create and mount a *ramdisk* on `/tmp`: the ramdisk created will be used to temporarily store the results of the usage while the script is running and avoid excessive I/O operations. The system is loaded using `sysbench` program (which is not provided here) and computing prime numbers.  

The results will be copied to this folder once each test is complete.

**In parallel** you should collect real power usage data from a DAQ. Scripts for doing this are not available at the moment.

All measurements are synchronized by relying on a local NTP server.

## Organize collected data
At this point, you should have the following CSV files:

  + `cpu_data_dump_N.csv`: Contains the system usage with CPU limited to N percentage. Each row is uniquely identified by the EPOCH time
  + `cpu_DATE.csv`: Contains the collected data on DATE from DAQ devices. Since we want to measure the power, the file will contain voltage and current measurements. Each row is uniquely identified by the EPOCH time.

We can run `data_cleaner.py` which will take care of computing the power from `cpu_DATE.csv` file, average that data (because the file contains 1000 measurements for each measurement in `cpu_data_dump_N.csv`), synchronize it with the ones from `cpu_data_dump_N.csv` and output a final file, `cpu_usage_vs_power.csv`, containing system usage vs. power consumption.

## Analyze the data
Data analysis is performed in python programming language (version 3). We make use of the following libraries:

  + *Pandas*: To read the CSV file and put it into *numpy* friendly data structures.
  + *Numpy*: To handle operations with arrays/matrices of numbers.
  + *Matplotlib*: To plot graphs.

The script in charge of performing the analysis is `data_analyzer.py`: it will make a scatter plot of the collected data, then will compute N-th order regression of such data and will output also the RMSE values of the functions.

Plot can be manually saved through the facilities offered by `Matplotlib.pyplot`. Here is an example:

![System Usage vs. Power Consumption](http://i.imgur.com/TKbtxf1.png)