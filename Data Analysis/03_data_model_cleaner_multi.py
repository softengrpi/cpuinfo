#!/usr/bin/env python3
#
# Author:   Marco Ardizzone
# Email:    marco.ardizzone@studenti.polito.it
# Date:     27-04-17
# Description:
#   This script is in charge of taking the data collected from the DAQ and the
#   cpu usages of the RPi and merge them together.
#   Before running the script you should point the variables to the right files
#   you acquired.
#   The variables that should be tuned are:
#       - input_DAQ_file => Should point to the file you acquired with the DAQ.
#         The DAQ file should contain 3 columns: time from Epoch (Jan 1, 1904),
#         current and voltage (this order must be enforced).
#       - output_final_file => Is the output file generated.
#       - input_usage_file => Is the input file containing the cpu usages from
#         the RPi.
#       - bus_state = Either 'on' or 'off'.
#   First the script reads the usage file and calculates the average frequency
#   and saves it, together with the usage and epoch inside usage_data variable.
#   In the same function (read_usage_file_multi) we also save into a list a tuple
#   containing the minimum and maximum epoch.
#   We then proceed to read the DAQ file and read from it only the values whose
#   epoch is in range of the min and max epochs calculated before. Since the
#   Epoch coming from the DAQ is not the same Epoch of the usage file a
#   conversion is performed first.
#   Once this is done, we compute the power from the current and the voltage and
#   save all this data to DAQ_data variable.
#   Due to the much higher sampling speed of the DAQ, we need to average the DAQ
#   data, so we first retrieve all the epochs from usage_data and then we average
#   the DAQ data according to these epochs: we match the epoch coming
#   from the usage with all the epochs of the DAQ lower than that one, we take
#   the corresponding power and average it.
#   Finally we merge together the usages and the powers by looping through the
#   variables we have filled in.
#   Results are output on the output file specified before and it contains the
#   epoch, usage, power and the frequency.
#################################################################################

import os
from math import floor

cur_dir = os.getcwd()
epoch_diff = 2082844800  # Difference between epoch Jan 1, 1904 and Jan 1, 1970
usage_data = {}
epochs_min_max = []
DAQ_data = list()  # List of tuples. Tuple contains (time, power)
avg_DAQ_data = dict()

bus_state = "off"
executor = "sysbench"
input_DAQ_file = "{}/daq_{}_bus_{}_monitor_on.csv".format(cur_dir, executor, bus_state)
input_usage_file = "{}/{}_{}_{}.csv".format(cur_dir, "cpu_data_dump", executor, bus_state)

output_final_file = "{}/cpu_usage_vs_power.csv".format(cur_dir)


def read_usage_file_multi():
    with open(input_usage_file, 'r') as fd_usage_file:
        # Skip the first 3 lines
        for skip in range(0, 3):
            next(fd_usage_file)
        # Loop through the lines of the file and get the time and usage value
        m = float('+inf')
        M = float('-inf')
        for line in fd_usage_file:
            values = line.split(";")
            # Save the time as a string because it's a float and we want to
            # use it as a key for usage_data
            time = values[0]
            # Calculate also the min and max epochs
            if float(time) < m:
                m = float(time)
            elif float(time) > M:
                M = float(time)
            usage_val = float(values[2])
            avg_frequency = (float(values[3]) + float(values[4]) +
                             float(values[5]) + float(values[6])) / 4
            usage_data[time] = (usage_val, avg_frequency)
        epochs_min_max.append((m, M))


def read_usage_file_single():
    '''
    Sysbench single core test. Don't use with the others
    '''
    with open(input_usage_file, 'r') as fd_usage_file:
        # Skip the first 3 lines
        for skip in range(0, 3):
            next(fd_usage_file)
        # Loop through the lines of the file and get the time and usage value
        m = float('+inf')
        M = float('-inf')
        for line in fd_usage_file:
            values = line.split(";")
            # Save the time as a string because it's a float and we want to
            # use it as a key for usage_data
            time = values[0]
            # Calculate also the min and max epochs
            if float(time) < m:
                m = float(time)
            elif float(time) > M:
                M = float(time)
            usage_val = max(values[3:7])
            avg_frequency = (float(values[7]) + float(values[8]) +
                             float(values[9]) + float(values[10])) / 4
            usage_data[time] = (usage_val, avg_frequency)
        epochs_min_max.append((m, M))


def read_DAQ_file():
    index_epochs = 0
    # is_in_range: Used to tell if we ever stepped into an epoch range inside
    # the DAQ file.
    # We use it also to understand when we step out of the range and know that
    # a new CPU % measure starts
    is_in_range = False
    with open(input_DAQ_file, 'r') as fd_DAQ_in:
        # Skip the header
        next(fd_DAQ_in)
        for line in fd_DAQ_in:
            values = line.split(";")
            # Convert EPOCH 1904 -> EPOCH 1970
            time = float(values[0].replace(",", "."))
            time -= epoch_diff
            # If time is in the range of the min and max epoch.
            # Max epoch +1 because time is float, so we must consider also
            # x.99..99 upper bound
            if time >= epochs_min_max[index_epochs][0] and time < (epochs_min_max[index_epochs][1] + 1):
                power = float(values[1].replace(",", ".")) * float(values[2].replace(",", "."))
                DAQ_data.append((time, power))
                # Trigger the flag, so we know we stepped into an epoch interval
                is_in_range = True
            elif is_in_range:
                is_in_range = False
                index_epochs += 1
                if index_epochs >= len(epochs_min_max):
                    break


def average_DAQ_data():
    usage_epochs = list()
    for time, val in usage_data.items():
        usage_epochs.append(time)
    usage_epochs = sorted(usage_epochs)

    epoch_index = 0
    for data in DAQ_data:
        # data[0] = epoch
        # data[1] = power
        if epoch_index >= len(usage_epochs):
            break

        epoch = usage_epochs[epoch_index]
        # If the key is still not there, it means we are running a new loop
        if epoch not in avg_DAQ_data:
            count = 0
            avg_DAQ_data[epoch] = 0.0
        if data[0] < float(epoch) + 0.001:
            avg_DAQ_data[epoch] += data[1]
            count += 1
        else:
            avg_DAQ_data[epoch] /= count
            epoch_index += 1


def make_output_file():
    '''
    usage_data[time] = (usg, freq)
        time => epoch
    '''

    from operator import itemgetter
    with open(output_final_file, 'w') as fd_out:
        fd_out.write("Time (s);Usage;Power (W);Avg Frequency (MHz)\n")
        data = []
        for time, usg_freq in usage_data.items():
            avg_pwr = avg_DAQ_data[time]
            data.append((time, usg_freq[0], avg_pwr, usg_freq[1] / 1000))
        data = sorted(data, key=itemgetter(0))
        for vals in data:
            fd_out.write("{};{};{};{}\n".format(vals[0], vals[1],
                                                vals[2], vals[3]))

#read_usage_file_multi()
read_usage_file_single()
read_DAQ_file()
average_DAQ_data()
make_output_file()
