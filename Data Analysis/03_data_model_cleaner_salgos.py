#!/usr/bin/env python3
#
# Author:   Marco Ardizzone
# Email:    marco.ardizzone@studenti.polito.it
# Date:     04-04-17
# Description:
#   This script is in charge of taking the data collected from the DAQ and the
#   cpu usages of the RPi and merge them together.
#   Before running the script you should point the variables to the right files
#   you acquired.
#   The variables that should be tuned are:
#       - input_DAQ_file => Should point to the file you acquired with the DAQ.
#         The DAQ file should contain 3 columns: time from Epoch (Jan 1, 1904),
#         current and voltage (this order must be enforced).
#       - basename_output_file => Is the output file generated.
#       - input_usage_files => Is an OrderedDict that has keys representing the
#         sorting algorithms used to run the test. The characters coming after
#         the '-' symbol represent the order of the original array supplied to
#         the sorting program. 'r' is a reversed array, 'r1' is a random 1 type
#         array, 'r2' is a random 2 type array.
#         In case you are supplying the files of the single-core analysis, they
#         should contain the following columns: Epoch (Jan 1, 1970),
#         iteration number, core used by sorting program, core 1 usage, core 2
#         usage, core 3 usage, core 4 usage, core frequency 1, core frequency 2,
#         core frequency 3 and core frequency 4 (this order must be enforced).
#         If instead the files belong to the multi-core analysis, these are the
#         required columns: Epoch (Jan 1, 1970), iteration number, usage, core
#         frequency 1, core frequency 2, core frequency 3, core frequency 4.
#         Note that usage_data keys must match the keys of input_usage_files
#         variable.
#       - bus_state = Either 'on' or 'off'.
#       - analysis = Either 'single' or 'multi'
#   First the script reads all the usage files and for each one of them
#   calculates the average frequency and saves it, together with the usage and
#   epoch inside its variable. In the same function (read_usage_files) we also
#   save into a list a tuples containing the minimum and maximum epoch of each
#   input usage file.
#   We then proceed to read the DAQ file and read from it only the values whose
#   epoch is in range of the min and max epochs calculated before. Since the
#   Epoch coming from the DAQ is not the same Epoch of the usage files
#   a conversion is performed first.
#   Once this is done, we compute the power from the current and the voltage and
#   save all this data to its variable.
#   Due to the much higher sampling speed of the DAQ, we need to perform a mean
#   on the DAQ data, so we first retrieve all the epochs from usage_data and then
#   we average the DAQ data according to these epochs: we match the epoch coming
#   from the usage with all the epochs of the DAQ lower than that one, we take
#   the corresponding power and average it.
#   Finally we merge together the usages and the powers by looping through the
#   variables we have filled in.
#   Results are output on several files (one for each key in usage_data) and they
#   contain the epoch, usage, power and the frequency.
#################################################################################

import os
from collections import OrderedDict
from math import floor
from sys import exit

cur_dir = os.getcwd()  # Current Working Directory
epochs_min_max = list()
DAQ_data = list()
avg_DAQ_data = dict()
epoch_diff = 2082844800  # Difference between epoch Jan 1, 1904 and Jan 1, 1970

bus_state = "on"     # Can be either 'on' or 'off'
analysis = "single"  # Can be either 'single' or 'multi'
input_DAQ_file = cur_dir + "/cpu_salgos_bus_{}_monitor_on.csv".format(bus_state)
basename_output_file = "cpu_usage_vs_power"
input_usage_files = OrderedDict([
                                ('quick-r',
                                 cur_dir +
                                 '/cpu_data_dump_quick_r_20000_{}.csv'.format(bus_state)),
                                ('quick-r1',
                                 cur_dir +
                                 '/cpu_data_dump_quick_r1_5000000_{}.csv'.format(bus_state)),
                                ('quick-r2',
                                 cur_dir +
                                 '/cpu_data_dump_quick_r2_5000000_{}.csv'.format(bus_state)),
                                ('merge-r',
                                 cur_dir +
                                 '/cpu_data_dump_merge_r_5000000_{}.csv'.format(bus_state)),
                                ('merge-r1',
                                 cur_dir +
                                 '/cpu_data_dump_merge_r1_5000000_{}.csv'.format(bus_state)),
                                ('merge-r2',
                                 cur_dir +
                                 '/cpu_data_dump_merge_r2_5000000_{}.csv'.format(bus_state)),
                                ('bubble-r',
                                 cur_dir +
                                 '/cpu_data_dump_bubble_r_200000_{}.csv'.format(bus_state)),
                                ('bubble-r1',
                                 cur_dir +
                                 '/cpu_data_dump_bubble_r1_20000_{}.csv'.format(bus_state)),
                                ('bubble-r2',
                                 cur_dir +
                                 '/cpu_data_dump_bubble_r2_20000_{}.csv'.format(bus_state)),
                                ('counting-r',
                                 cur_dir +
                                 '/cpu_data_dump_counting_r_22000000_{}.csv'.format(bus_state)),
                                ('counting-r1',
                                 cur_dir +
                                 '/cpu_data_dump_counting_r1_5000000_{}.csv'.format(bus_state)),
                                ('counting-r2',
                                 cur_dir +
                                 '/cpu_data_dump_counting_r2_5000000_{}.csv'.format(bus_state))])
usage_data = OrderedDict([
                        ('quick-r', dict()),
                        ('quick-r1', dict()),
                        ('quick-r2', dict()),
                        ('merge-r', dict()),
                        ('merge-r1', dict()),
                        ('merge-r2', dict()),
                        ('bubble-r', dict()),
                        ('bubble-r1', dict()),
                        ('bubble-r2', dict()),
                        ('counting-r', dict()),
                        ('counting-r1', dict()),
                        ('counting-r2', dict())])


def read_usage_files_single():
    '''
    Singlecore version
    '''
    # Loop thorugh each file and open it
    for key, path in input_usage_files.items():
        with open(path, 'r') as fd_usage_file:
            # Skip the first 3 lines
            for skip in range(0, 3):
                next(fd_usage_file)
            # Loop through the lines of the file and get the time and usage value
            m = float('+inf')
            M = float('-inf')
            for line in fd_usage_file:
                values = line.split(";")
                # Save the time as a string because it's a float and we want to
                # use it as a key for usage_data
                time = values[0]
                # Calculate also the min and max epochs
                if float(time) < m:
                    m = float(time)
                elif float(time) > M:
                    M = float(time)
                core = int(values[2])
                usage_val = float(values[core + 2])
                avg_frequency = (float(values[7]) + float(values[8]) +
                                 float(values[9]) + float(values[10])) / 4
                usage_data[key][time] = (usage_val, avg_frequency)
            epochs_min_max.append((m, M))


def read_usage_files_multi():
    '''
    Multicore version
    '''
    # Loop thorugh each file and open it
    for key, path in input_usage_files.items():
        with open(path, 'r') as fd_usage_file:
            # Skip the first 3 lines
            for skip in range(0, 3):
                next(fd_usage_file)
            # Loop through the lines of the file and get the time and usage value
            m = float('+inf')
            M = float('-inf')
            for line in fd_usage_file:
                values = line.split(";")
                # Save the time as a string because it's a float and we want to
                # use it as a key for usage_data
                time = values[0]
                # Calculate also the min and max epochs
                if float(time) < m:
                    m = float(time)
                elif float(time) > M:
                    M = float(time)
                usage_val = float(values[2])
                avg_frequency = (float(values[3]) + float(values[4]) +
                                 float(values[5]) + float(values[6])) / 4
                usage_data[key][time] = (usage_val, avg_frequency)
            epochs_min_max.append((m, M))


def read_DAQ_file():
    index_epochs = 0
    # is_in_range: Used to tell if we ever stepped into an epoch range inside
    # the DAQ file.
    # We use it also to understand when we step out of the range and know that
    # a new CPU % measure starts
    is_in_range = False
    with open(input_DAQ_file, 'r') as fd_DAQ_in:
        # Skip the header
        next(fd_DAQ_in)
        for line in fd_DAQ_in:
            values = line.split(";")
            # Convert EPOCH 1904 -> EPOCH 1970
            time = float(values[0].replace(",", "."))
            time -= epoch_diff
            # If time is in the range of the min and max epoch.
            # Max epoch +1 because time is float, so we must consider also
            # x.99..99 upper bound
            if time >= epochs_min_max[index_epochs][0] and time < (epochs_min_max[index_epochs][1] + 1):
                power = float(values[1].replace(",", ".")) * float(values[2].replace(",", "."))
                DAQ_data.append((time, power))
                # Trigger the flag, so we know we stepped into an epoch interval
                is_in_range = True
            elif is_in_range:
                is_in_range = False
                index_epochs += 1
                if index_epochs >= len(epochs_min_max):
                    break


def average_DAQ_data():
    usage_epochs = list()
    for vals in usage_data.values():
        for key, val in vals.items():
            usage_epochs.append(key)
    usage_epochs = sorted(usage_epochs)

    epoch_index = 0
    for data in DAQ_data:
        # data[0] = epoch
        # data[1] = power
        if epoch_index >= len(usage_epochs):
            break

        epoch = usage_epochs[epoch_index]
        # If the key is still not there, it means we are running a new loop
        if epoch not in avg_DAQ_data:
            count = 0
            avg_DAQ_data[epoch] = 0.0
        if data[0] < float(epoch) + 0.001:
            avg_DAQ_data[epoch] += data[1]
            count += 1
        else:
            avg_DAQ_data[epoch] /= count
            epoch_index += 1


def make_output_file():
    '''
    usage_data[key][time] = (usg, freq)
        key => 'quick-r', 'merge-r', 'bubble-r1', etc..
        time => epoch

    '''
    from operator import itemgetter
    for key, time_usg_freq in usage_data.items():
        with open("{}/{}_{}.{}".format(cur_dir, basename_output_file, key, "csv"), 'w') as fd_out:
            fd_out.write("Time;Usage; Power (W); Avg Frequency (MHz)\n")
            data = []
            for time, usg_freq in time_usg_freq.items():
                avg_pwr = avg_DAQ_data[time]
                data.append((time, usg_freq[0], avg_pwr, usg_freq[1] / 1000))
            data = sorted(data, key=itemgetter(0))
            for vals in data:
                fd_out.write("{};{};{};{}\n".format(vals[0], vals[1],
                                                    vals[2], vals[3]))

if analysis == 'multi':
    read_usage_files_multi()
elif analysis == 'single':
    read_usage_files_single()
else:
    print("Unknown type of analysis")
    exit(-1)
read_DAQ_file()
average_DAQ_data()
make_output_file()
