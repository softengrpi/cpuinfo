#!/usr/bin/env python3
#
# Author:   Marco Ardizzone
# Email:    marco.ardizzone@studenti.polito.it
# Date:     March 2017
# Description:
#   This script is in charge of taking the data collected from the DAQ and the
#   cpu usages of the RPi and merge them together.
#   Before running the script you should point the variables to the right files
#   you acquired.
#   The variables that should be tuned are:
#       - input_DAQ_file => Should point to the file you acquired with the DAQ.
#         The DAQ file should contain 3 columns: time from Epoch (Jan 1, 1904),
#         current and voltage (this order must be enforced).
#       - output_final_file => Is the output file generated. It will contain two
#         columns: Power and Usage.
#       - input_usage_files => Is an OrderedDict that has keys ranging from 10 to
#         100. You should set the value of each key to the corresponding cpu
#         usage file.
#         The files should contain the following columns: Epoch (Jan 1, 1970),
#         iteration number, utilization, frequency 1, frequency 2, frequency 3
#         and frequency 4 (this order must be enforced).
#         You can find this variable in read_usage_files() function.
#   First the script reads all the usage files and for each one of them calculates
#   the average frequency and saves it, together with the usage and epoch inside
#   its variable.
#   Then we look for the minimum and maximum epoch for each usage.
#   We then proceed to read the DAQ file and read from it only the values whose
#   epoch is in range of the min and max epochs calculated before. Since the
#   Epoch coming from the DAQ is not the same Epoch of the usage files
#   a conversion is performed first.
#   Once this is done, we compute the power from the current and the voltage and
#   save all this data to its variable.
#   Due to the much higher speed of the DAQ, we have 1000 times more measures
#   than the cpu usages, so we perform and average on these values, so that we
#   get into the same order of magnitude.
#   Finally we merge together the usages and the powers by looping through the
#   variables we have filled in. The results are output on a file.
#################################################################################

import os
from collections import OrderedDict
from math import floor

cur_dir = os.getcwd()
# Difference between epoch Jan 1, 1904 and Jan 1, 1970
epoch_diff = 2082844800

input_DAQ_file = cur_dir + "/daq_sysbench_busy_bus_on_rpi3.csv"

usage_data = OrderedDict()
usage_data[100] = dict()
usage_data[90] = dict()
usage_data[80] = dict()
usage_data[70] = dict()
usage_data[60] = dict()
usage_data[50] = dict()
usage_data[40] = dict()
usage_data[30] = dict()
usage_data[20] = dict()
usage_data[10] = dict()


output_final_file = cur_dir + "/cpu_usage_vs_power_load.csv"
# List of tuples. Tuple contains (time, power)
DAQ_data = list()


def read_usage_files():
    usage = 100  # Start index
    input_usage_files = OrderedDict()
    input_usage_files["100"] = cur_dir + "/cpu_data_dump_100.csv"
    input_usage_files["90"] = cur_dir + "/cpu_data_dump_90.csv"
    input_usage_files["80"] = cur_dir + "/cpu_data_dump_80.csv"
    input_usage_files["70"] = cur_dir + "/cpu_data_dump_70.csv"
    input_usage_files["60"] = cur_dir + "/cpu_data_dump_60.csv"
    input_usage_files["50"] = cur_dir + "/cpu_data_dump_50.csv"
    input_usage_files["40"] = cur_dir + "/cpu_data_dump_40.csv"
    input_usage_files["30"] = cur_dir + "/cpu_data_dump_30.csv"
    input_usage_files["20"] = cur_dir + "/cpu_data_dump_20.csv"
    input_usage_files["10"] = cur_dir + "/cpu_data_dump_10.csv"
    # Loop thorugh each file and open it
    for path in input_usage_files.values():
        with open(path, 'r') as fd_usage_file:
            # Skip the first 2 lines
            for skip in range(0, 3):
                next(fd_usage_file)
            # Loop through the lines of the file and get the time and usage value
            for line in fd_usage_file:
                values = line.split(";")
                time = int(values[0])
                usage_val = float(values[2])
                avg_freq = (float(values[3]) + float(values[4]) + float(values[5]) + float(values[6])) / 4
                usage_data[usage][time] = (usage_val, avg_freq)
        usage -= 10


def get_min_max_epoch_list():
    # Return a list of tuples containing the minimum and maximum epoch of each CPU %
    epoch_list = list()
    for usg_key in range(100, 9, -10):
        epoch_list.append((min(list(usage_data[usg_key].keys())),
                           max(list(usage_data[usg_key].keys()))))
    return epoch_list


def read_DAQ_file():
    index_epoch_list = 0
    # is_in_range: Used to tell if we ever stepped into an epoch range inside
    # the DAQ file.
    # We use it also to understand when we step out of the range and know that
    # a new CPU % measure starts
    is_in_range = False
    with open(input_DAQ_file, 'r') as fd_DAQ_in:
        # Skip the header
        next(fd_DAQ_in)
        for line in fd_DAQ_in:
            values = line.split(";")
            # Convert EPOCH 1904 -> EPOCH 1970
            time = float(values[0].replace(",", "."))
            time -= epoch_diff
            # If time is in the range of the min and max epoch.
            # Max epoch +1 because time is float, so we must consider also x.99..99 upper bound
            if time >= epoch_list[index_epoch_list][0] and time < (epoch_list[index_epoch_list][1] + 1):
                power = float(values[1].replace(",", ".")) * float(values[2].replace(",", "."))
                DAQ_data.append((time, power))
                is_in_range = True  # Trigger the flag, so we know we stepped into an epoch interval
            elif is_in_range:
                is_in_range = False
                index_epoch_list += 1
                if index_epoch_list >= len(epoch_list):
                    break


def average_DAQ_data():
    cur_floor = None
    count = 0
    output = dict()
    for data in DAQ_data:
        # data[0] = epoch
        # data[1] = power
        if not cur_floor:
            cur_floor = floor(data[0])
            output[cur_floor] = 0.0
        if cur_floor != floor(data[0]):
            output[cur_floor] /= count
            cur_floor = floor(data[0])
            count = 0
            output[cur_floor] = 0.0
        output[cur_floor] += data[1]
        count += 1
    output[cur_floor] /= count  # Divide also the last set of data
    return output


def make_output_file():
    with open(output_final_file, 'w') as fd_out:
        fd_out.write("Usage; Power (W); Avg Frequency (KHz)\n")
        for epoch_and_usage in usage_data.values():
            for epoch, usage in epoch_and_usage.items():
                avg_pwr = DAQ_data_averaged[epoch]
                fd_out.write("{};{};{}\n".format(usage[0], avg_pwr, usage[1]))


read_usage_files()
epoch_list = get_min_max_epoch_list()
read_DAQ_file()
DAQ_data_averaged = average_DAQ_data()
make_output_file()
