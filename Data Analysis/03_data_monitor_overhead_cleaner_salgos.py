#!/usr/bin/env python3
#
# Author:   Marco Ardizzone
# Email:    marco.ardizzone@studenti.polito.it
# Date:     07-04-17
# Description:
#   This script is in charge of calculating the average power consumption when
#   the monitor program is not running.
#   There are some variables that should be tuned before using the script:
#       - bus_state => Can be either 'on' or 'off'
#       - input_executor_logs => Each tuple should be adjusted to match the right
#         file. The executor logs should contain 2 strings indicating the start
#         and end time of the test. The strings should be formatted according to
#         'start_time_str' and 'end_time_str'.
#       - input_DAQ_file => Should point to the file you acquired with the DAQ.
#         The DAQ file should contain 3 columns: time from Epoch (Jan 1, 1904),
#         current and voltage (this order must be enforced).
#       - output_monitor_file => Is the name of the output file. It will have 2
#         columns: Algorithm and Average Power, indicating for each algorithm how
#         much power, on average, was used.
#   First the script opens the executor files and looks for the start and end
#   epochs and saves them to start_end_time_algos variable.
#   Then it will open the DAQ file, look for the current and voltage whose epoch
#   is in the range of the start_end_time_algos epochs, compute the power and,
#   for each algorithm, compute the average power. The result is saved to the
#   output file.
#################################################################################

import os
import re
from collections import OrderedDict
from math import floor

cur_dir = os.getcwd()
epoch_diff = 2082844800
start_end_times_algos = OrderedDict([
                                    ('quick-r', [-1, -1]),
                                    ('quick-r1', [-1, -1]),
                                    ('quick-r2', [-1, -1]),
                                    ('merge-r', [-1, -1]),
                                    ('merge-r1', [-1, -1]),
                                    ('merge-r2', [-1, -1]),
                                    ('bubble-r', [-1, -1]),
                                    ('bubble-r1', [-1, -1]),
                                    ('bubble-r2', [-1, -1]),
                                    ('counting-r', [-1, -1]),
                                    ('counting-r1', [-1, -1]),
                                    ('counting-r2', [-1, -1])
                                    ])
start_time_str = "Start time: (\d+)"
end_time_str = "End time: (\d+)"
start_regex = re.compile(start_time_str)
end_regex = re.compile(end_time_str)

bus_state = "on"
input_executor_logs = OrderedDict([
                                  ('quick-r',
                                   cur_dir + '/quick_6000000_2.csv'),
                                  ('quick-r1',
                                   cur_dir + '/quick_6000000_3.csv'),
                                  ('quick-r2',
                                   cur_dir + '/quick_6000000_4.csv'),
                                  ('merge-r',
                                   cur_dir + '/merge_8000000_2.csv'),
                                  ('merge-r1',
                                   cur_dir + '/merge_8000000_3.csv'),
                                  ('merge-r2',
                                   cur_dir + '/merge_8000000_4.csv'),
                                  ('bubble-r',
                                   cur_dir + '/bubble_50000_2.csv'),
                                  ('bubble-r1',
                                   cur_dir + '/bubble_50000_3.csv'),
                                  ('bubble-r2',
                                   cur_dir + '/bubble_50000_4.csv'),
                                  ('counting-r',
                                   cur_dir + '/counting_40000000_2.csv'),
                                  ('counting-r1',
                                   cur_dir + '/counting_40000000_3.csv'),
                                  ('counting-r2',
                                   cur_dir + '/counting_40000000_4.csv')])
input_DAQ_file = cur_dir + '/cpu_salgos_bus_{}_monitor_off.csv'.format(bus_state)
output_monitor_file = cur_dir + '/consumptions_bus_{}_monitor_off.csv'.format(bus_state)

for algo, path in input_executor_logs.items():
    start_time_matched = False
    with open(path, 'r') as fd_exec_log:
        for line in fd_exec_log:
            if start_time_matched:
                matcher = re.match(end_regex, line)
            else:
                matcher = re.match(start_regex, line)
            if matcher:
                if not start_time_matched:
                    start_time_matched = True
                    start_end_times_algos[algo][0] = matcher.group(1)
                else:
                    start_end_times_algos[algo][1] = matcher.group(1)

with open(input_DAQ_file, 'r') as fd_DAQ_log:
    next(fd_DAQ_log)  # Skip header line
    with open(output_monitor_file, 'w') as fd_output_file:
        fd_output_file.write("Algorithm;Average Power\n")
        for algo, start_end in start_end_times_algos.items():
            start = int(start_end[0])
            end = int(start_end[1])
            tot = 0
            count = 0
            in_range = False
            for line in fd_DAQ_log:
                values = line.split(";")
                time = float(values[0].replace(",", "."))
                time -= epoch_diff
                if time >= start and time < end + 1:
                    tot += float(values[1].replace(",", ".")) * float(values[2].replace(",", "."))
                    count += 1
                    in_range = True
                elif in_range:
                    fd_output_file.write("{};{}\n".format(algo, tot / count))
                    break
