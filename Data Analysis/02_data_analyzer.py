#!/usr/bin/env python3
#
# Author:   Marco Ardizzone
# Email:    marco.ardizzone@studenti.polito.it
# Date:     March 2017
# Description:
#   This script is in charge of taking the data generated with 01_data_cleaner*.py
#   scripts and produce a plot of such data.
#   The variables df_load and df_idle should be set up to point
#   to the right files.
#   The script will plot all the point and will calculate the linear regression.
#   Linear regression calculation is done up to the 4th order. You will likely
#   need to change the script a bit if you want to calculate a higher order
#   regression. Calculation of lower order regression may require no change but
#   max_degree variable which should be set to the desired order.
#   Please note that idle data is output to stdout and not on the plot.
# Dependencies:
#   The script requires some extra packages to work:
#       - Pandas: to read input csv file and save it to data structures easy to
#         handle.
#       - Numpy: to perform operations on matrices and arrays easily.
#       - Matplotlib: to plot on graphs all the points we collected.
#   All these packages can be installed using pip, but please refer to their
#   websites to know more about installation instructions.
#################################################################################

from pandas import read_csv
from bisect import bisect_left, bisect_right
import numpy as np
import matplotlib.pyplot as plt

screen_dpi = 96  # Should be adjusted according to the screen DPI of your PC
max_degree = 4
# Indicates the lower and upper bounds of the usage value used to calculate
# frequency means for each usage step
usage_threshold = 2


def index_ge(a, x):
    'Find the index of the leftmost item greater than or equal to x'
    i = bisect_left(a, x)
    if i != len(a):
        return i
    raise ValueError


def index_le(a, x):
    'Find the index of the rightmost value less than or equal to x'
    i = bisect_right(a, x)
    if i:
        return i - 1
    raise ValueError


def get_formula(coefficients):
    '''
        Coefficients are in a list ordered
        from the higher-order to the
        lower-order.

        Returns a string representing the formula
    '''
    formula = str()
    degree = len(coefficients) - 1
    for c in coefficients:
        if degree == 0:
            if c >= 0:
                formula += "+{0:.4f}".format(c)
            else:
                formula += "{0:.4f}".format(c)
        elif degree == 1:
            if c >= 0:
                formula += "+{0:.4f}$u$ ".format(c)
            else:
                formula += "{0:.4f}$u$ ".format(c)
        else:
            if c >= 0:
                formula += "+{0:.4f}$u^{1}$ ".format(c, degree)
            else:
                formula += "{0:.4f}$u^{1}$ ".format(c, degree)
        degree -= 1
    return "P($u$) = " + formula


df_load = read_csv("cpu_usage_vs_power_load.csv", sep=";",
                   header=0, names=["usage", "power", "freq"]
                   ).sort_values('usage')
# Usages are multiplied by 100 to have xaxis ranging from 0 to 100 rather than 0
# to 1
usages = df_load.get('usage') * 100
powers = df_load.get('power')
freqs = df_load.get('freq')
for usage_mark in range(10, 110, 10):
    lower_bound = index_le(usages.values.tolist(), usage_mark - usage_threshold)
    if usage_mark != 100:
        upper_bound = index_ge(usages.values.tolist(), usage_mark + usage_threshold)
    else:
        upper_bound = index_ge(usages.values.tolist(), usage_mark)
    if lower_bound == upper_bound:
        m = np.mean(freqs[lower_bound:])
    else:
        m = np.mean(freqs[lower_bound:upper_bound])
    print("Average frequency for {}%: {} MHz".format(usage_mark, m / 1000))

df_idle = read_csv("cpu_usage_vs_power_idle_off.csv",
                   sep=";", header=0, names=["usage", "power", "freq"]
                   ).sort_values('usage')
usages_idle = df_idle.get('usage') * 100
powers_idle = df_idle.get('power')
freqs_idle = df_idle.get('freq')

avg_idle_usg = np.mean(usages_idle)
avg_idle_pwr = np.mean(powers_idle)
avg_idle_frq = np.mean(freqs_idle)

df_combined = df_load.append(df_idle).sort_values('usage')
usages = df_combined.get('usage') * 100
powers = df_combined.get('power')

print("Idle data:")
print("\tUsage: {0: .2f}%".format(avg_idle_usg))
print("\tPower: {0: .4f} W\tRMSE: {1: .0f} mW".format(avg_idle_pwr, np.std(powers_idle) * 1000))
print("\tFrequency: {0: .0f} MHz".format(avg_idle_frq))

plt.title("Power consumption vs. CPU utilization")
plt.xlabel("CPU Utilization (%)")
plt.ylabel("Power Consumption (W)")
plt.scatter(usages, powers, marker='.', label="Collected data", color="#B7B7B7")
# Put a marker on x-axis every 10 units, from 0 to 100
plt.xticks(np.arange(0, 110, 10), np.arange(0, 110, 10))
# Limit the range of the x-axis from 0 to 105
plt.xlim((0, 105))

line_setup = {
    #   color, line style, line width
    #   Red
    1: ("#FF2828", "-", 2),
    #   Light Blue
    2: ("#5EC2FD", "--", 3),
    #   Gold
    3: ("#C0A753", "-.", 3),
    #   Dark Green
    4: ("#039C1F", "-", 2)}

# Contains function and RMSE of each regression
poly_data = "P($0$) = {0: .4f}\nRMSE: {1: .0f} mW\n\n".format(avg_idle_pwr, np.std(powers_idle) * 1000)
for degree in range(1, max_degree + 1):
    p = np.poly1d(np.polyfit(usages, powers, degree))
    p_predict = np.polyval(p, usages)
    p_error = np.mean((p_predict - powers)**2)
    p_rmse = np.sqrt(p_error)
    poly_data += "{}\nRMSE: {num:.{digits}f} mW\n\n".format(get_formula(p.c),
                                                            num=p_rmse * 1000,
                                                            digits=0)
    print(p)
    plt.plot(usages, p_predict,
             label="Regression order {}".format(degree),
             color=line_setup[degree][0],
             linestyle=line_setup[degree][1],
             linewidth=line_setup[degree][2])

# Position the data of the regression at x, y values of the axes.
# Ranges from (0,0) [bottom-left] to (1,1) [top-right]
plt.text(0.02, 0.95, poly_data, transform=plt.gca().transAxes, va="top",
         ha="left", fontsize=13)

# Add a legend, bottom right
plt.legend(loc="lower right")
cur_fig = plt.gcf()
# Show the plot
plt.show()

cur_fig.savefig('plot.png', dpi=screen_dpi)
