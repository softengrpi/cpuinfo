#!/usr/bin/env python3
#
# Author:   Marco Ardizzone
# Email:    marco.ardizzone@studenti.polito.it
# Date:     05-04-17
# Description:
#   This script takes as input the cpu_usage_vs_power_+ files and the
#   consumptions_bus_* files to calculate the predicted power and energy.
#   Everything is output both on screen and on a report csv file.
#   bus_state variable can be changed to 'on' or 'off' depending on the
#   configuration of the RPi. output_filename variable will indicate the output
#   file on which data will be put.
#   The scripts start by calculating the predicted power for each algorithm using
#   the equation provide inside get_predicted_power.
#   Once this is done, real and predicted energies are calculated from the powers
#   by looking at the difference between the end time and start time of the test.
#   When this calculation is performed, everything is saved onto the output file.
#   Output is also shown on screen.
#
#   Note: Be aware that this script requires pandas external library.
#   Please refer to its website for installation instructions.
#################################################################################

from sys import exit
from pandas import read_csv
from pathlib import Path

bus_state = "on"
regression_order = 4
output_filename = "energy_report_single_bus_{}-order-{}.csv".format(bus_state,
                                                                    regression_order)


def get_predicted_power(order, usage, monitor_overhead, dev_state=True):
    '''
      - order: Order of the prediction, based on the linear regressions
        computed inside 02_data_analyzer.py
      - usage: a float representing the usage of the CPU at a given time.
      - monitor_overhead: overhead power consumed by the monitor program
      - dev_state: True or False. Represents wether we want to compute the
        estimation with the regressions calculated when HDMI, Ethernet and
        USB were turned off or not.
        Defaults to True: devices are on.
    '''
    functions_on = {0: 1.7694,
                    1: 0.01048 * usage + 1.722,
                    2: -8.608e-06 * (usage**2) + 0.01135 * usage + 1.708,
                    3: -6.371e-07 * (usage**3) + 8.786e-05 * (usage**2) +
                    0.00758 * usage + 1.735,
                    4: 2.534e-08 * (usage**4) - 5.756e-06 * (usage**3) +
                    0.0004143 * (usage**2) + 0.0007165 * usage + 1.761}
    functions_off = {0: 1.2367,
                     1: 0.009565 * usage + 1.218,
                     2: -1.323e-05 * (usage**2) + 0.0109 * usage + 1.197,
                     3: -3.673e-07 * (usage**3) + 4.232e-05 * (usage**2) +
                     0.008732 * usage + 1.212,
                     4: 1.568e-08 * (usage**4) - 3.531e-06 * (usage**3) +
                     0.0002436 * (usage**2) + 0.004519 * usage + 1.228}
    if order > 4:
        print("Error: Maximum supported order is {}".format(1))
        exit(-1)
    if dev_state:
        return functions_on[order]
    else:
        return functions_off[order]

dataframes = {'quick-r': "cpu_usage_vs_power_quick-r.csv",
              'quick-r1': "cpu_usage_vs_power_quick-r1.csv",
              'quick-r2': "cpu_usage_vs_power_quick-r2.csv",
              'merge-r': "cpu_usage_vs_power_merge-r.csv",
              'merge-r1': "cpu_usage_vs_power_merge-r1.csv",
              'merge-r2': "cpu_usage_vs_power_merge-r2.csv",
              'bubble-r': "cpu_usage_vs_power_bubble-r.csv",
              'bubble-r1': "cpu_usage_vs_power_bubble-r1.csv",
              'bubble-r2': "cpu_usage_vs_power_bubble-r2.csv",
              'counting-r': "cpu_usage_vs_power_counting-r.csv",
              'counting-r1': "cpu_usage_vs_power_counting-r1.csv",
              'counting-r2': "cpu_usage_vs_power_counting-r2.csv"}

df_monitor = read_csv("consumptions_bus_{}_monitor_off.csv".format(bus_state),
                      sep=";", header=0, index_col=0, names=["avg_pwr"])


for df_key, df_file in dataframes.items():
    dataframe = read_csv(df_file, sep=";", header=0,
                         names=["time", "usage", "power", "frequency"]).sort_values('usage')
    usages = dataframe.get('usage') * 100
    powers = dataframe.get('power')
    times = [min(dataframe.get("time")), max(dataframe.get("time"))]
    pwr_wo_monitor = df_monitor.at[df_key, 'avg_pwr']
    count = 0
    tot_predicted = 0
    tot_real = 0
    for usage, power in zip(usages, powers):
        if bus_state == "on":
            predicted = get_predicted_power(regression_order, usage, power - pwr_wo_monitor, True)
        elif bus_state == "off":
            predicted = get_predicted_power(regression_order, usage, power - pwr_wo_monitor, False)
        else:
            print("Invalid bus state")
            exit(-1)
        count += 1
        tot_predicted += predicted
        tot_real += power - (power - pwr_wo_monitor)
    tot_predicted /= count
    tot_real /= count
    energy_real = tot_real * round(times[1] - times[0])
    energy_predicted = tot_predicted * round(times[1] - times[0])
    difference = energy_predicted * 100 / energy_real - 100
    output_file = Path(output_filename)
    if not output_file.is_file():
        with open(output_filename, 'w') as fd_out:
            fd_out.write("Test;Real P.(W);Predicted P.(W);Real E.(J);Predicted E.(J);Difference(%);Run Time(s)\n")
            fd_out.write(
                "{test} - Bus {0};{1: .3f};{2: .3f};{3: .3f};{4: .3f};{5: .3f};{6}\n"
                .format(bus_state,
                        tot_real,
                        tot_predicted,
                        energy_real,
                        energy_predicted,
                        difference,
                        round(times[1] - times[0]),
                        test=df_key))
    else:
        with open(output_filename, 'a') as fd_out:
            fd_out.write(
                "{test} - Bus {0};{1: .3f};{2: .3f};{3: .3f};{4: .3f};{5: .3f};{6}\n"
                .format(bus_state,
                        tot_real,
                        tot_predicted,
                        energy_real,
                        energy_predicted,
                        difference,
                        round(times[1] - times[0]),
                        test=df_key))
    print("Results for {}".format(df_key))
    print("Real Power\t\t=\t{0: .3f} W".format(tot_real))
    print("Predicted Power\t\t=\t{0: .3f} W".format(tot_predicted))
    print("Real Energy\t\t=\t{0: .3f} J".format(energy_real))
    print("Predicted Energy\t=\t{0: .3f} J".format(energy_predicted))
    print("Percentage Difference\t=\t{0: .3f} %".format(difference))
    print("Run time\t=\t{0} s".format(round(times[1] - times[0])))
