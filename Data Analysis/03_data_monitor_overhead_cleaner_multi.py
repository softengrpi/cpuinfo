#!/usr/bin/env python3
#
# Author:   Marco Ardizzone
# Email:    marco.ardizzone@studenti.polito.it
# Date:     27-04-17
# Description:
#   This script is in charge of calculating the average power consumption when
#   the monitor program is not running.
#   There are some variables that should be tuned before using the script:
#       - bus_state => Can be either 'on' or 'off'
#       - input_executor_log => File containing the start and end time of the
#         Executor test. It should contain a header and then the start and end
#         epoch only.
#       - input_DAQ_file => Should point to the file you acquired with the DAQ.
#         The DAQ file should contain 3 columns: time from Epoch (Jan 1, 1904),
#         current and voltage (this order must be enforced).
#       - output_monitor_file => Is the name of the output file. It will have 2
#         columns: Algorithm and Average Power, indicating for each algorithm how
#         much power, on average, was used.
#   First the script opens the executor file and looks for the start and end
#   epochs and saves them to start_end variable.
#   Then it will open the DAQ file, look for the current and voltage whose epoch
#   is in the range of the start_end epochs, compute the power and, compute the
#   average power. The result is saved to the output file.
#################################################################################

import os
import re
from math import floor

cur_dir = os.getcwd()
epoch_diff = 2082844800
start_end = []

bus_state = "on"
executor = "openssl"
input_executor_log = "{}/executor_{}_bus_{}_monitor_off.csv".format(cur_dir, executor, bus_state)
input_DAQ_file = "{}/daq_{}_bus_{}_monitor_off.csv".format(cur_dir, executor, bus_state)
output_monitor_file = "{}/consumptions_bus_{}_monitor_off.csv".format(cur_dir, bus_state)

start_time_str = "Start time: (\d+)"
end_time_str = "End time: (\d+)"
start_regex = re.compile(start_time_str)
end_regex = re.compile(end_time_str)

start_time_matched = False
with open(input_executor_log, 'r') as fd_exec_log:
    for line in fd_exec_log:
        if start_time_matched:
            matcher = re.match(end_regex, line)
        else:
            matcher = re.match(start_regex, line)
        if matcher:
            if not start_time_matched:
                start_time_matched = True
                start_end.append(int(matcher.group(1)))
            else:
                start_end.append(int(matcher.group(1)))

with open(output_monitor_file, 'w') as fd_output_file:
    fd_output_file.write("{} power consumption\n".format(executor))
    with open(input_DAQ_file, 'r') as fd_DAQ_log:
        next(fd_DAQ_log)  # Skip header line
        start = int(start_end[0])
        end = int(start_end[1])
        tot = 0
        count = 0
        in_range = False
        for line in fd_DAQ_log:
            values = line.split(";")
            time = float(values[0].replace(",", "."))
            time -= epoch_diff
            if time >= start and time < end + 1:
                tot += float(values[1].replace(",", ".")) * float(values[2].replace(",", "."))
                count += 1
                in_range = True
            elif in_range:
                fd_output_file.write("{}\n".format(tot / count))
                break
