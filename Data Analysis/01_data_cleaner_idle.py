#!/usr/bin/env python3
#
# Author:   Marco Ardizzone
# Email:    marco.ardizzone@studenti.polito.it
# Date:     March 2017
# Description:
#   This script is in charge of taking the data collected from the DAQ and the
#   cpu usages of the RPi and merge them together.
#   Before running the script you should point the variables to the right files
#   you acquired.
#   The variables that should be tuned are:
#       - input_DAQ_file => Should point to the file you acquired with the DAQ.
#         The DAQ file should contain 3 columns: time from Epoch (Jan 1, 1904),
#         current and voltage (this order must be enforced).
#       - basename_output_file => Is the output file generated. It will contain
#         two columns: Power and Usage.
#       - input_usage_files => Is an OrderedDict that has keys 'off', 'off-hdmi',
#         'off-usb' and 'on'. They represent the state in which the RPi was when
#         the idle values were sampled. 'Off' is when Ethernet, USB and HDMI were
#         turned off. 'Off-usb' is when Ethernet and USB were turned off.
#         'Off-hdmi' is when HDMI was turned off.
#         You should set the value of each key to the corresponding cpu usage
#         file.
#         The files should contain the following columns: Epoch (Jan 1, 1970),
#         iteration number, utilization, frequency 1, frequency 2, frequency 3
#         and frequency 4 (this order must be enforced).
#         You can find this variable in read_usage_files() function.
#   First the script read all the usage files and for each one of them calculates
#   the average frequency and saves it, together with the usage and epoch inside
#   its variable.
#   Then we look for the minimum and maximum epoch for each usage.
#   We then proceed to read the DAQ file and read from it only the values whose
#   epoch is in range of the min and max epochs calculated before. Since the
#   Epoch coming from the DAQ is not the same Epoch of the usage files
#   a conversion is performed first.
#   Once this is done, we compute the power from the current and the voltage and
#   save all this data to its variable.
#   Due to the much higher speed of the DAQ, we have 1000 times more measures
#   than the cpu usages, so we perform and average on these values, so that we
#   get into the same order of magnitude.
#   Finally we merge together the usages and the powers by looping through the
#   variables we have filled in. The results are output on a file.
#################################################################################

import os
from collections import OrderedDict
from math import floor

cur_dir = os.getcwd()
epoch_diff = 2082844800

input_DAQ_file = cur_dir + "/daq_r3_idle.csv"

usage_data = OrderedDict()
usage_data['off-usb'] = dict()
usage_data['off-hdmi'] = dict()
usage_data['off'] = dict()
usage_data['on'] = dict()

epoch_list = list()

basename_output_file = "cpu_usage_vs_power_idle"

# List of tuples. Tuple contains (time, power)
DAQ_data = list()

avg_DAQ_data = dict()


def read_usage_files():
    input_usage_files = OrderedDict()
    input_usage_files["off-usb"] = cur_dir + "/cpu_data_dump_idle_off-usb.csv"
    input_usage_files["off-hdmi"] = cur_dir + "/cpu_data_dump_idle_off-hdmi.csv"
    input_usage_files["off"] = cur_dir + "/cpu_data_dump_idle_off.csv"
    input_usage_files["on"] = cur_dir + "/cpu_data_dump_idle_on.csv"
    # Loop thorugh each file and open it
    for key, path in input_usage_files.items():
        with open(path, 'r') as fd_usage_file:
            # Skip the first 3 lines
            for skip in range(0, 3):
                next(fd_usage_file)
            # Loop through the lines of the file and get the time and usage value
            for line in fd_usage_file:
                values = line.split(";")
                time = int(values[0])
                usage_val = float(values[2])
                avg_frequency = (float(values[3]) + float(values[4]) + float(values[5]) + float(values[6])) / 4
                usage_data[key][time] = (usage_val, avg_frequency)


def get_min_max_epoch_list():
    # Return a list of tuples containing the minimum and maximum epoch of each CPU %
    for key, time_and_vals in usage_data.items():
        epoch_list.append((min(list(time_and_vals.keys())),
                           max(list(time_and_vals.keys()))))


def read_DAQ_file():
    index_epoch_list = 0
    # is_in_range: Used to tell if we ever stepped into an epoch range inside
    # the DAQ file.
    # We use it also to understand when we step out of the range and know that
    # a new CPU % measure starts
    is_in_range = False
    with open(input_DAQ_file, 'r') as fd_DAQ_in:
        # Skip the header
        next(fd_DAQ_in)
        for line in fd_DAQ_in:
            values = line.split(";")
            # Convert EPOCH 1904 -> EPOCH 1970
            time = float(values[0].replace(",", "."))
            time -= epoch_diff
            # If time is in the range of the min and max epoch.
            # Max epoch +1 because time is float, so we must consider also x.99..99 upper bound
            if time >= epoch_list[index_epoch_list][0] and time < (epoch_list[index_epoch_list][1] + 1):
                power = float(values[1].replace(",", ".")) * float(values[2].replace(",", "."))
                DAQ_data.append((time, power))
                is_in_range = True  # Trigger the flag, so we know we stepped into an epoch interval
            elif is_in_range:
                is_in_range = False
                index_epoch_list += 1
                if index_epoch_list >= len(epoch_list):
                    break


def average_DAQ_data():
    cur_floor = None
    count = 0
    for data in DAQ_data:
        # data[0] = epoch
        # data[1] = power
        if not cur_floor:
            cur_floor = floor(data[0])
            avg_DAQ_data[cur_floor] = 0.0
        if cur_floor != floor(data[0]):
            avg_DAQ_data[cur_floor] /= count
            cur_floor = floor(data[0])
            count = 0
            avg_DAQ_data[cur_floor] = 0.0
        avg_DAQ_data[cur_floor] += data[1]
        count += 1
    avg_DAQ_data[cur_floor] /= count  # Divide also the last set of data


def make_output_file():
    for key, time_usg_freq in usage_data.items():
        with open("{}_{}.{}".format(basename_output_file, key, "csv"), 'w') as fd_out:
            fd_out.write("Usage; Power (W); Avg Frequency (MHz)\n")
            for time, usg_freq in time_usg_freq.items():
                avg_pwr = avg_DAQ_data[time]
                fd_out.write("{};{};{}\n".format(usg_freq[0], avg_pwr, usg_freq[1] / 1000))

read_usage_files()
get_min_max_epoch_list()
read_DAQ_file()
print(len(DAQ_data))
#average_DAQ_data()
#make_output_file()
