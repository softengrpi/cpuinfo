#!/usr/bin/env Rscript

maxDegree = 4
pointColor = "#B7B7B7"
lineColor = c("#FF2828", "#5EC2FD", "#C0A753", "#039C1F")
lineType = c(   1,  # continuous line
                2,  # --
                4,  # -.
                6   # dashed long-short
            )
lineWidth = c(2, 3, 3, 2)
legendText = c("Regression order 1", "Regression order 2", "Regression order 3", "Regression order 4")

data = read.csv('acquired logs/cpu_usage_vs_power.csv', sep=";")
colnames(data) = c("usage","power")
data = data[order(data["usage"]),]
# Convert usages to percentage
data["usage"] = data["usage"]*100

# Save the plot to JPG
jpeg("acquired logs/plot_R.jpg", width=1920, height=1080)
# Plot the data but avoid plotting the x-axis
plot(data, main="CPU Usage vs. Power consumption", xlab="CPU Usage (%)", ylab="Power Consumption (W)", xaxt="n", col=pointColor)
# Use a custom x-axis
axis(1, at=seq(0, 100, 10))

for (degree in 1:maxDegree) {
    model = lm(power ~ poly(usage, degree, raw=T), data=data)
    predicted = fitted(model)
    lines(predicted ~ data[,"usage"], lwd=lineWidth[degree], lty=lineType[degree], col=lineColor[degree])
    rmse = sqrt( sum( (predicted - data[,"power"])^2) / nrow(data) )
    cat("Coefficients of polynomial of degree", degree, "\n")
    cat(0:degree, sep="\t\t")
    cat("\n")
    for (c in coef(model)) {
        cat(c,"\t")
    }
    cat("\n")
    cat("RMSE:", rmse, "\n\n")
}

# Change font size for legend
par(cex=1.5)
legend(80, 1.8, legendText, col=lineColor, lty=lineType, lwd=lineWidth)