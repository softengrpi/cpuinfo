# How to perform the measurements
This is a step-by-step guide on how you can perform the same measurements I did with the RPi, with any other model of RPi.

## Initial Setup
This section explains how each device should be setup (hardware and software) before starting the measurements.

###Raspberry Pi - Software
The RPi should run Raspbian and should have the following packages installed:

  + CPULimit (https://github.com/opsengine/cpulimit)
  + Sysbench (https://github.com/akopytov/sysbench)
  + NTPDate (Should be already installed)
  + Avconv (Part of Libav package https://libav.org/)
  + Zip (Can be installed through `apt-get`)
  + Openssl (Can be installed through `apt-get`)
  + Python 3 (Can be installed through `apt-get`)

Once all these programs are installed, you can clone this repository to the RPi.

###Raspberry Pi - Hardware
Due to the high sensitivity of the system, even a slight movement of the setup may change the base idle current measured by the DAQ. For this reason, I suggest you to plug **before starting any test** a keyboard (to control the RPi), an HDMI cable (to see what's going on) and a USB Male-Female to which you will connect a USB key to get the saved dumps once each test is finished.

The extension USB cable is there to minimize any abrupt movement when (un)plugging the USB key: the RPi USB ports are quite tight and it's hard to remove a USB device without applying to much pressure and moving the RPi.

###PC and DAQ
The setup here should be pretty straightforward. You need to have Labview installed and load the project DAQ RPi. The project is setup to measure current on input 17-18 and voltage on input 19-20. Inside the project you just need to choose a name for the file containing the DAQ measures (see action `Save to ASCII/LVM` on the sidebar of Labview).

Additionally, you have to install NetTime (http://www.timesynctool.com/) and set as a primary NTP server `127.0.0.1`. This program is used to synch the time between the RPi and the PC so that the data coming from the DAQ (voltage and current) can be compared to the data coming from the RPi (CPU usage).

## Measuring
This section describes the different phases for creating a power model of the RPi. This part is divided into two subsections: the first for the model generation and the second for the model test.

###Model Generation
All the meaningful files for this sections are in the repository folder `01_Generate Model/`. Each file has an in-depth description of how it works and what it does, here we are giving just a brief explanation. Please refer to these descriptions to better understand the details of each program.

The folder contains the following files:

  + `01_make-all.sh` is a script for compiling cpulimit and the monitor for sampling the CPU usage. It takes a `clean` parameter to remove compiled files.
  + `02_run-tests-busy.sh` is what is used to load the CPU and generate the model. Takes as input:
     - bus state (`on` or `off`). If you want to create a model with HDMI, Ethernet and USB off or on.
     - Root password : needed to mount a RAMDisk on which the dumps will be temporarily saved and perform other root operations.
     - NTP server address: The IPv4 address of the PC on which you installed NetTime. The RPi and this PC must be on the same network.

    At the beginning of the script there is a set of configurable parameters:

        # Change as needed
        MONITOR_PROG="monitor.out"      # Program that monitors the cpu usage
        CPULIMIT_PROG="cpulimit.out"    # Program to limit CPU to a certain maximum percentage
        LOADER_PROG="sysbench"          # Program used to load the CPU
        TEST_TIME=900                   # How long should we run the test for? (seconds)

    They are pretty much self-explanatory

  + `02_run-tests-idle.sh` has the same purpose of the previous script but measures the idle usage of the RPi. Also this script has a set of configurable parameters. They are no different with respect to the previous ones.
  This script doesn't take the bus state as input argument.
  + `monitor.c` is the program used to sample the usages. You should not run it yourself because it is run by the two previous script with the correct number of parameters.

Here is how to run the scripts:

  1. Make sure `01_Generate Model/monitor.c` and `cpulimit/` exist.
  2. Run `01_make-all.sh`, it will compile `monitor.out` and `cpulimit.out` binary files.
  3. On the DAQ PC name the DAQ file as you wish and press Record.
  4. On the RPi, run `02_run-tests-busy.sh` with the parameters as described before. E.g:

        ./02_run-tests-busy.sh on raspberry 192.168.1.101
    
    This script will output 10 `cpu_data_dump*.csv` files containing the usage of the CPU limited by cpulimit to the number you find the in the dump filename.

  5. Run `02_run-tests-idle.sh` with the paramters as described before. E.g:

        ./02_run-tests-idle.sh raspberry 192.168.1.101
    
    The script will generate 4 `cpu_data_dump*.csv` files: `off`, `on`, `off-hdmi` and `off-usb`.

  6. Refer to the Data Analysis part.

###Model Testing
All the meaningful files for this sections are in the repository folder `02_Test Model/`. Each file has an in-depth description of how it works and what it does, here we are giving just a brief explanation. Please refer to these descriptions to better understand the details of each program.

Since I wanted to be thorought in testing the reliability of the generated model, this part involves running single-core tests and multi-core tests. For this reason, one of the programs (sorting algorithm program) used to load the CPU and the `monitor` used to sample the usage have 2 different versions: one single-core and one multi-core. These versions are located in to two separate repositories which you can find here:

  + [Monitor Repository](https://bitbucket.org/softengrpi/monitor-cpu)
  + [Loader Repository](https://bitbucket.org/softengrpi/loader-cpu)

The folder contains the following files:

  + `loader/` is the loader folder which should contain only the source files of the sorting algorithm program (see repository above). Copy here the source files of the multi-core or single-core algorithm based on which type of test you want to perform.
  + `01_make-all.sh` is a script for compiling the monitor for sampling the CPU usage and the loader (if available). It takes a `clean` parameter to remove compiled files.
  + `02_run-tests-busy.sh` is the main file used to run the tests. This are the required command-line arguments:
    - `dev_state`: on or off . If off HDMI, Ethernet and USB are turned off before starting the test.
    - `monitor_state`: on or off . If off monitor won't be run.
    - `psw`: password for super user
    - `ntp_server_address`: The IPv4 address of the PC on which you installed NetTime. The RPi and this PC must be on the same network.
    
    This script has a lot of configurable parameters:

        # Change as needed
        MONITOR_PROG="monitor.out"      # Program that monitors the cpu usage
        LOADER_PROG="sysbench"          # Program used to load the CPU. Choices: sysbench, avconv, zip, openssl, executor.out
        RUN_TIME=600                    # Seconds to run sysbench
        VIDEO_FILE="burgers.mp4"        # Video to be used by the run_test_video, can be mp4 or mkv format
        DUMMY_FILE="dummy.txt"          # File used for zip and openssl tests
        TEST_TYPE_CORE="multi"         # Can be multi or single. multi = multicore test, single = single core test
    
      - `LOADER_PROG` can be one of the following choices: `sysbench`, `avconv`, `zip`, `openssl` or `executor.out`.
      - `zip` and `openssl` are architecture-independent tests. So the `TEST_TYPE_CORE` has no relevance. On a multi-core system they will use multiple cores, otherwise they will use one core only. `avconv` can be set to use one single core: unfortunately the documentation is rather complex and I haven't found a way to do so. By default it behaves just as `zip` and `openssl`.
      - `RUN_TIME` parameter is relevant only if `LOADER_PROG` is set to sysbench. It indicates how much time sysbench should run for.
      - `VIDEO_FILE` points to a video file in mp4 or mkv format. This is what is used by `avconv` test. The repository provides 2 video files, `burgers.mp4` and `berry.mp4`: the difference between them is the length. `berry.mp4` is most suited for CPUs with a low computational power. In theory, you could use any video file you want, as long as it is mp4 or mkv format.
      - `DUMMY_FILE` is just a big text file used by `openssl` and `zip` test. You can use whatever plain-text file you wish but be careful that the smaller the text file, the quicker the test will be and you may not be able to sample quickly enough. 
      - `TEST_TYPE_CORE` parameter is relevant only when `LOADER_PROG` is `executor.out` or `sysbench`. These are the only single-core tests available. **NOTE:** If you are using `executor.out` (the sorting algorithm program) as `LOADER_PROG`, you must make sure that you are using the correct version (multi or single core).

    Additionally, if you are using the sorting algorithm program as a test, you can change the size and initial ordering of the array at the bottom of the script. E.g:

        run_test_order "quick" 6000000 2 ...

    Is a quicksort on a *6,000,000* elements array, which are in reverse order (2).

  + `monitor.c` is the program used to sample the usages. You should not run it yourself because it is run by the two previous script with the correct number of parameters.
  **NOTE:** Please remember to use the right version of the monitor based on the type of test you are doing (multi-core or single-core).

Here is how to run the scripts:

  1. Make sure `02_Test Model/monitor.c` and `02_Test Model/loader/*.(c|h)` exist. Also **make sure the source code matches the type of test you want to do (multi or single core)** 
  2. Run `01_make-all.sh`. It will compile `monitor.out` and, if needed, `executor.out` binary files.
  3. On the DAQ PC name the DAQ file as you wish and press Record.
  4. On the RPi, run `02_run-tests-busy.sh` with the parameters as described before. E.g:

        ./02_run-tests-busy.sh on on raspberry 192.168.1.101
    
    This script will output a number of `cpu_data_dump*.csv` files containing the usage of the CPU.
    The script will also output (*in some cases*) `executor` files which are the output of the executor program used.

  5. For each `LOADER_PROG` you use, you should run step 4 at least 4 times, to have all the possible states of bus and monitor:
    - Bus On, Monitor On
    - Bus Off, Monitor On
    - Bus Off, Monitor Off
    - Buss On, Monitor Off

    And remember that some test are both multi core and single core, so in some cases the test to be done are 8.

  6. Refer to the Data Analysis part.

