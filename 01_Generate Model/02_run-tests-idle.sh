#!/usr/bin/env bash
#
# Author:   Marco Ardizzone
# Email:    marco.ardizzone@studenti.polito.it
# Date:     March 2017
# Description:
#   This is the main script used to run the tests when the CPU is in idle state.
#   The script takes care of doing a number of checks before starting sampling
#   data.
#   You need to provide two arguments to run the script: <root_psw> and
#   <ntp_server_address>.
#   The program will check if the NTP program to synchronize time is installed on
#   the system and if so it will synchronize the time with the NTP server.
#   It will check then if the monitoring program is compiled.
#   If all the checks are successful, it will try to mount a ramdisk to /tmp/
#   folder.
#   Once this procedure is over, the idle tests will be run from run_test
#   function, which takes as input a string indicating in which state should the
#   system devices be. Possible states are:
#       - 'off': Ethernet, USB and HDMI are turned off before running the test.
#       - 'off-hdmi': HDMI is turned off before running the test.
#       - 'off-usb': Ethernet and USB are turned off before running the test.
#       - 'on': The tests are run with all devices on.
#   run_test function takes care of checking if the provided string is valid and
#   then will try synchronizing time again, turn devices off if needed, run the
#   monitor in wait mode, start the monitor and stop the test when the time is
#   up. Stopping the test means sending termination signal (SIGUSR2) to the
#   monitor that will save the dump of the CPU usage.
#   
#   There is a number of configurable parameters down below, you can change them
#   as needed.
#   
#   Generally, in case of errors, the script should be able to stop everything
#   and restore the system state to its original condition (i.e turn HDMI,
#   ethernet and USB ports on if they were turned off).
#   Some instances of the launched programs may still be running in background if
#   an error occurs, so a system reboot is suggested.
#################################################################################
#
# DO NOT TOUCH
# This fetches the controller in charge of managing the usb and the ethernet. It's specific for the RPi
ETH_CONTROLLER=$(find /sys/devices/ -name `dmesg -t | grep dwc_otg | grep "DWC OTG Controller" | awk '{print $2}' | cut -d":" -f1`)
NTP_SERVER=                     # Local NTP server used to sync hour 
RAMDISK=''
CUR_DIR=$(pwd)                  # The current working directory of the script

# Change as needed
MONITOR_PROG="monitor.out"      # Program that monitor the cpu usage
TEST_TIME=900                   # How long should we run the test for? (seconds)

function print_help(){
    case $1 in
        "args")
            echo
            echo "$0 <psw> <ntp_server_address>"
            echo
            echo "Where:"
            echo "  - <psw>: password for super user"
            echo "  - <ntp_server_address>: IPv4 address of the local NTP server used to sync the time"
            echo
            ;;
        "monitor")
            echo
            echo "Monitor program $MONITOR_PROG was not found"
            echo "Maybe you need to compile it?"
            echo
            ;;
        "ntpdate")
            echo
            echo "ntpdate was not found"
            echo "Maybe you need to install it?"
            echo
            ;;
        *)
            echo
            echo "Something went terribly wrong"
            echo
            ;;
    esac
}

#Creates and mounts the ramdisk on which we'll save the results of the monitoring
function make_ramdisk(){
    RAMDISK="/tmp/ramdisk`date +%s`"
    mkdir $RAMDISK || return 1
    echo $1 | sudo -S mount -t ramfs -o size=128m ramfs $RAMDISK || return 1
    sudo chmod 777 $RAMDISK || return 1
    echo "Ramdisk mounted successfully at $RAMDISK"
    return 0
}

function set_HDMI(){
    if [[ $1 == "off" ]]; then
        echo -n "Disabling HDMI.. "
        sleep 10
        echo $psw | sudo -S /opt/vc/bin/tvservice -o
        echo "Done"
    elif [[ $1 == "on" ]]; then
        echo $psw | sudo -S /opt/vc/bin/tvservice -p
        # The following is a hack found https://www.raspberrypi.org/forums/viewtopic.php?t=16472&p=176258
        # The problem with reactivating the HDMI is that the screen remains blank even if blanking is
        # turned off with setterm -blank 0, pressing any key does nothing.
        # So chvt changes the foreground console to 6 (a random one) and back to 1 (the one used on the RPi
        # HDMI terminal output).
        echo $psw | sudo -S chvt 6
        echo $psw | sudo -S chvt 1
        echo "HDMI enabled"
    else
        echo "Wrong HDMI set parameter. Supply either 'on' or 'off'"
        return 1
    fi
    return 0
}

function set_eth_usb_controller(){
    if [[ $1 == "off" ]]; then
        echo "Stopping controller, see you later!"
        echo $psw | sudo -S ip link set eth0 down
        echo $psw | sudo -S /etc/init.d/networking stop
        echo 0 | sudo tee $ETH_CONTROLLER/buspower
        echo "Controller stopped"
    elif [[ $1 == "on" ]]; then
        echo "Enabling controller"
        echo 1 | sudo tee $ETH_CONTROLLER/buspower
        sleep 2
        echo $psw | sudo -S /etc/init.d/networking start
        echo $psw | sudo -S ip link set eth0 up
        echo "Controller enabled"
    else
        echo "Wrong Ethernet+USB controller set parameter. Supply either 'on' or 'off'"
        return 1
    fi
    return 0
}

function do_test(){

    echo "Running idle test: $1"
    ./"$MONITOR_PROG" "$RAMDISK" $1 &
    monitor_pid=$(pgrep $MONITOR_PROG)
    #Start the monitor
    kill -s SIGUSR1 $monitor_pid

    sleep $TEST_TIME

    echo -n "Killing $MONITOR_PROG... "
    while [[ $(pgrep $MONITOR_PROG) == $monitor_pid ]]; do
        kill -s SIGUSR2 $monitor_pid
        sleep 0.5
    done
    echo "Done"
}

function run_test(){
    # Sometimes sync doesn't work. Whether or not we are able to sync time,
    # move on.
    # Time is synched before running the test, so at least we have a baseline.
    echo $psw | sudo -S ntpdate -u $NTP_SERVER > /dev/null 2>&1
    case $1 in
        "off" )
            set_HDMI "off"
            set_eth_usb_controller "off"

            do_test "off"

            set_HDMI "on"
            set_eth_usb_controller "on"
            ;;
        "off-usb" )
            set_eth_usb_controller "off"

            do_test "off-usb"

            set_eth_usb_controller "on"
            ;;
        "off-hdmi" )
            set_HDMI "off"

            do_test "off-hdmi"

            set_HDMI "on"
            ;;
        "on" )
            do_test "on"
            ;;
        * )
            echo "Unknown run_test paramter"
            echo "Something went terribly wrong"
            return 1
            ;;
    esac
    if mv $RAMDISK/* "$CUR_DIR/" ; then
        echo "All files copied to $CUR_DIR"
        echo
        echo
    else
        echo "Copy of files failed, do it yourself."
        echo "Files are at $RAMDISK"
        echo
        echo
        return 1
    fi
    return 0
}

#If the arguments are not 2 print help and exit
if [[ "$#" -ne 2 ]]; then
    print_help "args"
    exit 1
else
    psw=$1          #Save root password
    NTP_SERVER=$2   #Save NTP server address

    #Check if ntpdate is installed, if not exit
    type ntpdate >/dev/null 2>&1 || { print_help "ntpdate"; exit 3;}
    #Kill ntp daemon so there's no interference in the epoch sync
    echo $psw | sudo -S kill -s SIGTERM $(pgrep ntpd)
    #Check if the NTP server is valid and reachable, if not exit
    echo $psw | sudo -S ntpdate -u $NTP_SERVER > /dev/null 2>&1|| { echo "Cannot find NTP server $NTP_SERVER"; exit 4; }

    # If the monitor program is not found, print help and exit
    if [[ ! -e "$MONITOR_PROG" ]]; then
        print_help "monitor"
        exit 5
    fi
    #Make the ramdisk
    make_ramdisk $psw || { echo "Ramdisk creation failed!"; exit 7; }
    #Disable screen blanking
    setterm -blank 0
fi

run_test "off-usb" || { set_HDMI "on"; set_eth_usb_controller "on"; exit 6; }
run_test "off-hdmi" || { set_HDMI "on"; set_eth_usb_controller "on"; exit 6; }
run_test "off" || { set_HDMI "on"; set_eth_usb_controller "on"; exit 6; }
run_test "on" || { set_HDMI "on"; set_eth_usb_controller "on"; exit 6; }
