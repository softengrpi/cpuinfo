#!/usr/bin/env bash
#
# Author:   Marco Ardizzone
# Email:    marco.ardizzone@studenti.polito.it
# Date:     March 2017
# Description:
#   This is the main script used to run the tests when the CPU is under a given
#   load.
#   The script takes care of doing a number of checks before starting sampling
#   data.
#   You need to provide three arguments to run the script: <dev_state>,
#   <root_psw> and <ntp_server_address>.
#   The program will check if the NTP program to synchronize time is installed on
#   the system and if so it will synchronize the time with the NTP server.
#   It will check then if the program used to load the cpu (sysbench) is
#   installed together as checking if the monitoring program and cpulimit are
#   installed/compiled.
#   If all the checks are successful, it will try to mount a ramdisk to /tmp/
#   folder.
#   Once this procedure is over, the tests will be run from run_test function,
#   which takes as input the limit percentage of the cpu that will be used by
#   cpulimit.
#   run_test function takes care of checking if the provided limit is a valid one
#   and then will try synchronizing time again, turn devices off if needed
#   (see <dev_state> parameter), run sysbench, run cpulimit, run the monitor in
#   wait mode, start the monitor and stop the test when the time is up. Stopping
#   the test means killing sysbench and cpulimit as well as sending termination
#   signal (SIGUSR2) to the monitor that will save the dump of the CPU usage.
#   
#   There is a number of configurable parameters down below, you can change them
#   as needed.
#   
#   Generally, in case of errors, the script should be able to stop everything
#   and restore the system state to its original condition (i.e turn HDMI,
#   ethernet and USB ports on if they were turned off).
#   Some instances of the launched programs may still be running in background if
#   an error occurs, so a system reboot is suggested.
#   
#   Down below you can find a link to a script for running cpulimit as a watchdog
#   to limit resources system-wide. It was initially used in the tests but then
#   it was dropped due to the amount of overhead it was creating.
#   
#   
# cpulimit watchdog taken from (not used, left here for reference only):
#   https://ubuntuforums.org/showthread.php?t=992706&s=16a90cc99b4c58932baf69caf00cf247
#
#################################################################################
#
# DO NOT TOUCH
# This fetches the controller in charge of managing the usb and the ethernet. It's specific for the RPi
ETH_CONTROLLER=$(find /sys/devices/ -name `dmesg -t | grep dwc_otg | grep "DWC OTG Controller" | awk '{print $2}' | cut -d":" -f1`)
CUR_DIR=$(pwd)                  # The current working directory of the script
RAMDISK=                        # Path to where data will be temporarly saved
NTP_SERVER=                     # Local NTP server used to sync hour 
DEVS_STATE=                     # Wheter Ethernet, HDMI and USB are on or off

# Change as needed
MONITOR_PROG="monitor.out"      # Program that monitors the cpu usage
CPULIMIT_PROG="cpulimit.out"    # Program to limit CPU to a certain maximum percentage
LOADER_PROG="sysbench"          # Program used to load the CPU
TEST_TIME=900                   # How long should we run the test for? (seconds)

function print_help(){
    case $1 in
        "args")
            echo
            echo "$0 <dev_state> <psw> <ntp_server_address>"
            echo
            echo "Where:"
            echo "  - <dev_state>: on or off . If off HDMI, Ethernet and USB are turned off before starting the test."
            echo "  - <psw>: password for super user"
            echo "  - <ntp_server_address>: IPv4 address of the local NTP server used to sync the time"
            echo
            ;;
        "monitor")
            echo
            echo "Monitor program $MONITOR_PROG was not found"
            echo "Maybe you need to compile it?"
            echo
            ;;
        "sysbench")
            echo
            echo "sysbench was not found"
            echo "Maybe you need to install it?"
            echo
            ;;
        "ntpdate")
            echo
            echo "ntpdate was not found"
            echo "Maybe you need to install it?"
            echo
            ;;
        "cpulimit")
            echo
            echo "cpulimit program $CPULIMIT_PROG was not found"
            echo "Maybe you need to compile it?"
            echo
            ;;
        "devs")
            echo
            echo "Unknown device state $DEVS_STATE"
            echo "Possible states are 'on' or 'off'"
            echo
            ;;
        *)
            echo
            echo "Something went terribly wrong"
            echo
            ;;
    esac
}

#Creates and mounts the ramdisk on which we'll save the results of the monitoring
function make_ramdisk(){
    RAMDISK="/tmp/ramdisk`date +%s`"
    mkdir $RAMDISK || return 1
    echo $1 | sudo -S mount -t ramfs -o size=128m ramfs $RAMDISK || return 1
    sudo chmod 777 $RAMDISK || return 1
    echo "Ramdisk mounted successfully at $RAMDISK"
    return 0
}

#The function takes as input the CPU limit we want to use for the test 
function run_test(){

    cores=$(nproc)
    actual_limit=0

    #The argument must be in range [10;90]
    if [[ $1 == "" || "$1" -lt "10" || "$1" -gt "100" ]]; then
        echo
        echo "Please provide a valid cpu limit (%)"
        echo
        return 1
    fi

    #cpulimit uses a percentage value based on the number of cores. So we need to multiply the percentage by the number of cores we have
    let actual_limit=$1*$cores

    echo "Running test with CPU limited to $1%"

    ( sysbench --num-threads=$cores --test=cpu --cpu-max-prime=2000000 run & ) > /dev/null 2>&1
    loader_pid=$(pgrep $LOADER_PROG)

    if [[ "$1" -ne 100  ]]; then
        ./$CPULIMIT_PROG -p $loader_pid -l $actual_limit -z &
    fi
    # Sync time just before starting
    # If synching fails, it's ok. We've already synched before starting the tests,
    # so at least the error difference is minimum.
    echo $psw | sudo -S ntpdate -u $NTP_SERVER > /dev/null 2>&1

    if [[ "$DEVS_STATE" == "off" ]]; then
        set_eth_usb_controller "off"
        set_HDMI "off"
    fi

    ./"$MONITOR_PROG" "$RAMDISK" "$1" &
    monitor_pid=$(pgrep $MONITOR_PROG)

    #Start the monitor
    kill -s SIGUSR1 $monitor_pid

    SECONDS=0
    while [[ "$SECONDS" -lt "$TEST_TIME" ]]; do
        sleep 1
    done

    echo -n "Killing $MONITOR_PROG... "
    while [[ $(pgrep $MONITOR_PROG) == $monitor_pid ]]; do
        kill -s SIGUSR2 $monitor_pid
        sleep 0.5
    done
    echo "Done"

    echo -n "Killing $LOADER_PROG... "
    while [[ $(pgrep $LOADER_PROG) == $loader_pid ]]; do
        kill -s SIGTERM $loader_pid
        sleep 0.5
    done
    echo "Done"

    echo -n "Killing $CPULIMIT_PROG... "
    while [[ $(pgrep $CPULIMIT_PROG) != "" ]]; do
        kill -s SIGTERM $(pgrep $CPULIMIT_PROG)
        sleep 0.5
    done
    echo "Done"

    if [[ "$DEVS_STATE" == "off" ]]; then
        set_HDMI "on"
        set_eth_usb_controller "on"
    fi

    if mv $RAMDISK/* "$CUR_DIR/" ; then
        echo "All files copied to $CUR_DIR"
        echo
        echo
        return 0
    else
        echo "Copy of files failed, do it yourself."
        echo "Files are at $RAMDISK"
        echo
        echo
        return 1
    fi
}

function set_HDMI(){
    if [[ $1 == "off" ]]; then
        echo -n "Disabling HDMI.. "
        sleep 10
        echo $psw | sudo -S /opt/vc/bin/tvservice -o
        echo "Done"
    elif [[ $1 == "on" ]]; then
        echo $psw | sudo -S /opt/vc/bin/tvservice -p
        # The following is a hack found https://www.raspberrypi.org/forums/viewtopic.php?t=16472&p=176258
        # The problem with reactivating the HDMI is that the screen remains blank even if blanking is
        # turned off with setterm -blank 0, pressing any key does nothing.
        # So chvt changes the foreground console to 6 (a random one) and back to 1 (the one used on the RPi
        # HDMI terminal output).
        echo $psw | sudo -S chvt 6
        echo $psw | sudo -S chvt 1
        echo "HDMI enabled"
    else
        echo "Wrong HDMI set parameter. Supply either 'on' or 'off'"
        return 1
    fi
    return 0
}

function set_eth_usb_controller(){
    if [[ $1 == "off" ]]; then
        echo "Stopping controller, see you later!"
        echo $psw | sudo -S ip link set eth0 down
        echo $psw | sudo -S /etc/init.d/networking stop
        echo 0 | sudo tee $ETH_CONTROLLER/buspower
        echo "Controller stopped"
    elif [[ $1 == "on" ]]; then
        echo "Enabling controller"
        echo 1 | sudo tee $ETH_CONTROLLER/buspower
        sleep 2
        echo $psw | sudo -S /etc/init.d/networking start
        echo $psw | sudo -S ip link set eth0 up
        echo "Controller enabled"
    else
        echo "Wrong Ethernet+USB controller set parameter. Supply either 'on' or 'off'"
        return 1
    fi
    return 0
}

#If the arguments are not 2 print help and exit
if [[ "$#" -ne 3 ]]; then
    print_help "args"
    exit 1
else
    DEVS_STATE=$1   #Save the state of the devices
    if [[ "$DEVS_STATE" != "on" && "$DEVS_STATE" != "off" ]]; then
        print_help "devs"
        exit 8
    fi
    psw=$2          #Save the root password
    NTP_SERVER=$3   #Save the NTP server address

    #Check if sysbench is installed, if not exit
    type sysbench >/dev/null 2>&1 || { print_help "sysbench"; exit 2;}

    #Check if ntpdate is installed, if not exit
    type ntpdate >/dev/null 2>&1 || { print_help "ntpdate"; exit 3;}
    #Kill ntp daemon so there's no interference in the epoch sync
    echo $psw | sudo -S kill -s SIGTERM $(pgrep ntpd)
    #Check if the NTP server is valid and reachable, if not exit
    echo $psw | sudo -S ntpdate -u $NTP_SERVER > /dev/null 2>&1|| { echo "Cannot find NTP server $NTP_SERVER"; exit 4; }

    # If the monitor program is not found, print help and exit
    if [[ ! -e "$MONITOR_PROG" ]]; then
        print_help "monitor"
        exit 5
    fi

    # If cpulimit is not found, print help and exit
    if [[ ! -e "$CPULIMIT_PROG" ]]; then
        print_help "cpulimit"
        exit 6
    fi
    #Make the ramdisk
    make_ramdisk $psw || { echo "Ramdisk creation failed!"; exit 7; }
    #Disable screen blanking
    setterm -blank 0
fi

#Run all the needed test with different load. In case of errors, stop and exit
run_test 100 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 6; }
run_test 90 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 6; }
run_test 80 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 6; }
run_test 70 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 6; }
run_test 60 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 6; }
run_test 50 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 6; }
run_test 40 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 6; }
run_test 30 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 6; }
run_test 20 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 6; }
run_test 10 || { if [[ "$DEVS_STATE" == "off" ]]; then set_HDMI "on"; set_eth_usb_controller "on"; fi; exit 6; }
