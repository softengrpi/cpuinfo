#!/usr/bin/env bash
#
# Author:   Marco Ardizzone
# Email:    marco.ardizzone@studenti.polito.it
# Date:     March 2017
# Description:
#   This is a simple bash script for compiling the required files to run the
#   tests. It compiles the monitoring program and cpulimit (to limit the cpu time
#   given to a certain program).
#   Calling the script with 'clean' parameter deletes the compiled files.
#   
#   Down below you can find a number of configurable paramters which you should
#   change as needed for proper compilation.
#################################################################################

MONITOR_SRC="monitor.c"
MONITOR_BIN="monitor.out"
CPULIMIT_FOLDER="../cpulimit"
CPULIMIT_BIN="cpulimit.out"
ROOT_DIR=$(pwd)

if [[ $1 == "clean" ]]; then
    rm "$ROOT_DIR/$MONITOR_BIN"
    rm "$ROOT_DIR/$CPULIMIT_BIN"
    cd "$CPULIMIT_FOLDER" && make clean
else
    #Compile the monitor
    gcc -O3 -o $MONITOR_BIN $MONITOR_SRC

    #Compile cpulimit
    cd "$CPULIMIT_FOLDER" && make
    cp "$ROOT_DIR/$CPULIMIT_FOLDER/src/cpulimit.out" "$ROOT_DIR/$CPULIMIT_BIN"
fi